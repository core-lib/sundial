package io.sundial.engine;

import io.sundial.core.lifecycle.Lifecycle;
import io.sundial.engine.exception.ShuttingException;
import io.sundial.engine.exception.StartingException;

/**
 * 引擎接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:53
 */
public interface Engine extends Lifecycle {

    /**
     * 引擎状态
     *
     * @return 引擎状态
     */
    Status status();

    /**
     * 引擎启动
     *
     * @throws StartingException 引擎关闭异常
     */
    void startup() throws StartingException;

    /**
     * 引擎关闭
     *
     * @throws ShuttingException 引擎关闭异常
     */
    void shutdown() throws ShuttingException;

    /**
     * 调度器状态
     */
    enum Status {
        /**
         * 未初始化
         */
        WAITING("waiting"),
        /**
         * 启动中
         */
        STARTING_UP("starting up"),
        /**
         * 运行中
         */
        RUNNING("running"),
        /**
         * 关闭中
         */
        SHUTTING_DOWN("shutting down"),
        /**
         * 已终止
         */
        TERMINATED("terminated");

        public final String value;

        Status(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
