package io.sundial.core.lifecycle.exception;

import io.sundial.core.lifecycle.LifecycleException;

/**
 * 销毁异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 14:15
 */
public class DestroyingException extends LifecycleException {
    private static final long serialVersionUID = 2610225199592647219L;

    public DestroyingException() {
    }

    public DestroyingException(String message) {
        super(message);
    }

    public DestroyingException(String message, Throwable cause) {
        super(message, cause);
    }

    public DestroyingException(Throwable cause) {
        super(cause);
    }
}
