package io.sundial.core.event;

/**
 * 事件源
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 0:11
 */
public interface EventSource extends EventListenable<Event> {

    /**
     * 手动触发指定类型的事件
     *
     * @param event 事件
     */
    void fire(Event event);

    /**
     * 关闭事件源触发执行器
     */
    void shut();

}
