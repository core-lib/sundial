package io.sundial.application.impl;

import io.sundial.application.Application;
import io.sundial.scheduler.impl.ReactingScheduler;

/**
 * 框架应用
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 20:31
 */
public class StandaloneApplication extends ReactingScheduler implements Application {

}
