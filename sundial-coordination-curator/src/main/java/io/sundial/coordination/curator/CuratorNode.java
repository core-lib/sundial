package io.sundial.coordination.curator;

import io.sundial.coordination.node.Node;
import org.apache.zookeeper.data.Stat;
import org.assertj.core.util.Preconditions;

/**
 * ZooKeeper 节点
 *
 * @author Payne 646742615@qq.com
 * 2019/1/5 10:52
 */
public class CuratorNode implements Node {
    private final String path;
    private final byte[] data;
    private final Stat stat;

    public CuratorNode(String path, byte[] data, Stat stat) {
        Preconditions.checkNotNull(path, "path must not be null");
        Preconditions.checkNotNull(data, "data must not be null");
        Preconditions.checkNotNull(stat, "stat must not be null");
        this.path = path;
        this.data = data;
        this.stat = stat;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public byte[] getData() {
        return data;
    }

    @Override
    public int getVersion() {
        return stat.getVersion();
    }

    @Override
    public String toString() {
        return path;
    }
}
