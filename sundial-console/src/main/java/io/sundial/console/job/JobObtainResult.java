package io.sundial.console.job;

import io.sundial.console.ApiResult;
import io.sundial.job.JobDefinition;

/**
 * 作业获取结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 14:21
 */
public class JobObtainResult extends ApiResult {
    private JobDefinition jobDefinition;

    public JobDefinition getJobDefinition() {
        return jobDefinition;
    }

    public void setJobDefinition(JobDefinition jobDefinition) {
        this.jobDefinition = jobDefinition;
    }
}
