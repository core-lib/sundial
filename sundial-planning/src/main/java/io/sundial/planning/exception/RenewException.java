package io.sundial.planning.exception;

import io.sundial.planning.PlannerException;

/**
 * 恢复异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 17:57
 */
public class RenewException extends PlannerException {
    private static final long serialVersionUID = 3292121956219195684L;

    public RenewException() {
    }

    public RenewException(String message) {
        super(message);
    }

    public RenewException(String message, Throwable cause) {
        super(message, cause);
    }

    public RenewException(Throwable cause) {
        super(cause);
    }
}
