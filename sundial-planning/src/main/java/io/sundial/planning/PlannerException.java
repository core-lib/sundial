package io.sundial.planning;

import io.sundial.core.SundialException;

/**
 * 任务规划者异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 17:54
 */
public class PlannerException extends SundialException {
    private static final long serialVersionUID = -1325002085127493865L;

    public PlannerException() {
    }

    public PlannerException(String message) {
        super(message);
    }

    public PlannerException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlannerException(Throwable cause) {
        super(cause);
    }
}
