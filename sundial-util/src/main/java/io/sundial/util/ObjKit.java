package io.sundial.util;

/**
 * 对象工具类
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 12:40
 */
public abstract class ObjKit {

    public static ClassLoader getClassLoader(ClassLoader defaultClassLoader) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader == null) {
            classLoader = ClassLoader.getSystemClassLoader();
        }
        if (classLoader == null) {
            classLoader = defaultClassLoader;
        }
        return classLoader;
    }

}
