package io.sundial.console.task.core;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 计划
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 11:32
 */
@Entity
@Table(name = "qrtz_job_details")
public class Plan implements Serializable {
    private static final long serialVersionUID = 6441751442399229338L;

    @Id
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "job_name")),
            @AttributeOverride(name = "group", column = @Column(name = "job_group"))
    })
    private Key key;

    @Column(name = "description")
    private String description;

    @Embeddable
    public static class Key implements Serializable {
        private static final long serialVersionUID = -324231987655753799L;

        private String name;
        private String group;

        public Key() {
        }

        public Key(String name, String group) {
            this.name = name;
            this.group = group;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (name != null ? !name.equals(key.name) : key.name != null) return false;
            return group != null ? group.equals(key.group) : key.group == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (group != null ? group.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "" + group + "/" + name;
        }
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plan plan = (Plan) o;

        return key != null ? key.equals(plan.key) : plan.key == null;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public String toString() {
        return key != null ? key.toString() : null;
    }
}
