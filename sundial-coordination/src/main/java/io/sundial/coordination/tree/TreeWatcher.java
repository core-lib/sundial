package io.sundial.coordination.tree;

/**
 * 树形监听器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 16:30
 */
public interface TreeWatcher {

    /**
     * 当观察到节点树变化时回调，但不包括根节点。
     *
     * @param event 节点树变化事件
     */
    void onWatched(TreeEvent event) throws Exception;

}
