package io.sundial.core;

/**
 * 委派接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 21:41
 */
public interface Delegate<T> {

    T get();

}
