package io.sundial.repository;

import io.sundial.core.SundialException;

/**
 * 数据仓储异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/18 9:53
 */
public class RepositoryException extends SundialException {
    private static final long serialVersionUID = 203149089265128552L;

    public RepositoryException() {
    }

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
