package io.sundial.console.taskgroup.web;

import io.sundial.console.ApiParam;
import io.sundial.console.taskgroup.core.Period;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * 任务组创建参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 16:43
 */
public class TaskgroupCreateParam extends ApiParam {
    /**
     * 任务组名称
     */
    @NotEmpty(message = "任务组名称不能为空")
    @Size(max = 36, message = "任务组名称长度不能超过36个字符")
    @Pattern(regexp = "[^/;\\\\]+", message = "任务组名称不能包含[/;\\]")
    private String name;

    /**
     * 任务组调度周期
     */
    @NotNull(message = "任务组调度周期不能为空")
    private Period period;

    /**
     * 任务组调度周期表达式
     */
    @Size(max = 36, message = "任务组调度周期表达式长度不能超过36个字符")
    private String expression;

    /**
     * 任务组描述
     */
    @Size(max = 240, message = "任务组描述长度不能超过240个字符")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
