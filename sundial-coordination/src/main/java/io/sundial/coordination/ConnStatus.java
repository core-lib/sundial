package io.sundial.coordination;


/**
 * 连接状态
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 11:29
 */
public enum ConnStatus {

    CONNECTED(true),

    SUSPENDED(false),

    RECONNECTED(true),

    LOST(false),

    READONLY(true);

    public final boolean connected;

    ConnStatus(boolean connected) {
        this.connected = connected;
    }

}
