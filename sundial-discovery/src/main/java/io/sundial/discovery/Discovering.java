package io.sundial.discovery;

/**
 * 作业发现
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:34
 */
public interface Discovering {

    /**
     * 安装一个作业发现者，如果作业发现者已存在或安装失败则返回{@code false}，否则返回{@code true}
     *
     * @param discoverer 作业发现者
     * @return 安装成功：true 否则：false
     */
    boolean install(Discoverer discoverer);

    /**
     * 卸载一个作业发现者，如果作业发现者不存在或卸载失败则返回{@code false}，否则返回{@code true}
     *
     * @param discoverer 作业发现者
     * @return 卸载成功：true 否则：false
     */
    boolean uninstall(Discoverer discoverer);

}
