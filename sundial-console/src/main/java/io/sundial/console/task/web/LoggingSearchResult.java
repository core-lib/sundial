package io.sundial.console.task.web;

import io.sundial.console.ApiResult;
import io.sundial.console.task.dto.LoggingVO;

import java.util.List;

/**
 * 执行记录搜索结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/14 13:31
 */
public class LoggingSearchResult extends ApiResult {
    /**
     * 分页查询参数
     */
    private LoggingSearchParam param;
    /**
     * 总匹配条数
     */
    private long total;
    /**
     * 当前分页条目列表
     */
    private List<LoggingVO> items;

    public LoggingSearchParam getParam() {
        return param;
    }

    public void setParam(LoggingSearchParam param) {
        this.param = param;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<LoggingVO> getItems() {
        return items;
    }

    public void setItems(List<LoggingVO> items) {
        this.items = items;
    }
}
