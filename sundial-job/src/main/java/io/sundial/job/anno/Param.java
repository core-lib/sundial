package io.sundial.job.anno;

import java.lang.annotation.*;

/**
 * 作业参数注解
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 21:45
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
@Documented
public @interface Param {

    /**
     * 参数文案，对应{@link io.sundial.job.JobParameter#label}
     *
     * @return 参数文案
     */
    String value();

    /**
     * 参数编辑器，对应{@link io.sundial.job.JobParameter#editor}
     *
     * @return 参数编辑器
     */
    String editor() default "";

    /**
     * 参数描述，对应{@link io.sundial.job.JobParameter#description}
     *
     * @return 参数描述
     */
    String description() default "";

    /**
     * 字符串形式的参数可选项，形如 first-value:first-label,second-value:second-label,...
     * 如果{@link Param#options()}有值时，该值被忽略。
     *
     * @return 参数可选项
     */
    String option() default "";

    /**
     * 参数可选项，对应{@link io.sundial.job.JobParameter#options}
     *
     * @return 参数可选项
     */
    Option[] options() default {};

}
