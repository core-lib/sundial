package io.sundial.core;


/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:06
 */
public interface AtomicVTCommand<E extends Exception> extends AtomicCommand {

    void run() throws E;

}
