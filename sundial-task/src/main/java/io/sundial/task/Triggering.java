package io.sundial.task;

import java.io.Serializable;
import java.util.Date;

/**
 * 触发记录
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 16:27
 */
public class Triggering implements Serializable {
    private static final long serialVersionUID = 3274765626429770352L;

    // region 所属调度
    private String taskName;
    private String taskGroup;
    private Long time;
    // endregion

    // region 作业分片
    private Integer shardingIndex;
    private String shardingValue;
    // endregion

    private String schedulerName;
    private String executorName;

    private Status status;
    private Integer code;
    private String message;
    private String result;

    private Date dateCreated;
    private Date lastUpdated;
    private Integer version;

    public Triggering() {
    }

    public Triggering(Triggering.Key key) {
        this.taskName = key.taskName;
        this.taskGroup = key.taskGroup;
        this.time = key.time;

        this.shardingIndex = key.shardingIndex;
    }

    public Triggering(Scheduling scheduling, Integer shardingIndex, String shardingValue, String schedulerName, String executorName) {
        this.taskName = scheduling.getTaskName();
        this.taskGroup = scheduling.getTaskGroup();
        this.time = scheduling.getTime();
        this.shardingIndex = shardingIndex;
        this.shardingValue = shardingValue;
        this.schedulerName = schedulerName;
        this.executorName = executorName;
    }

    public static class Key implements Serializable {
        private static final long serialVersionUID = -3763847321056923484L;

        private final String taskName;
        private final String taskGroup;
        private final Long time;
        private final Integer shardingIndex;

        public Key(String taskName, String taskGroup, Long time, Integer shardingIndex) {
            this.taskName = taskName;
            this.taskGroup = taskGroup;
            this.time = time;
            this.shardingIndex = shardingIndex;
        }

        public String getTaskName() {
            return taskName;
        }

        public String getTaskGroup() {
            return taskGroup;
        }

        public Long getTime() {
            return time;
        }

        public Integer getShardingIndex() {
            return shardingIndex;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (taskName != null ? !taskName.equals(key.taskName) : key.taskName != null) return false;
            if (taskGroup != null ? !taskGroup.equals(key.taskGroup) : key.taskGroup != null) return false;
            if (time != null ? !time.equals(key.time) : key.time != null) return false;
            return shardingIndex != null ? shardingIndex.equals(key.shardingIndex) : key.shardingIndex == null;
        }

        @Override
        public int hashCode() {
            int result = taskName != null ? taskName.hashCode() : 0;
            result = 31 * result + (taskGroup != null ? taskGroup.hashCode() : 0);
            result = 31 * result + (time != null ? time.hashCode() : 0);
            result = 31 * result + (shardingIndex != null ? shardingIndex.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "/" + taskGroup + "/" + taskName + "/" + time + "/" + shardingIndex;
        }
    }

    public enum Status {
        /**
         * 已提交
         */
        SUBMITTED,
        /**
         * 已拒绝
         */
        REJECTED,
        /**
         * 已取消
         */
        CANCELED,
        /**
         * 执行中
         */
        EXECUTING,
        /**
         * 已完成
         */
        COMPLETED,
        /**
         * 已失败
         */
        FAILED,
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskGroup() {
        return taskGroup;
    }

    public void setTaskGroup(String taskGroup) {
        this.taskGroup = taskGroup;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getShardingIndex() {
        return shardingIndex;
    }

    public void setShardingIndex(Integer shardingIndex) {
        this.shardingIndex = shardingIndex;
    }

    public String getShardingValue() {
        return shardingValue;
    }

    public void setShardingValue(String shardingValue) {
        this.shardingValue = shardingValue;
    }

    public String getSchedulerName() {
        return schedulerName;
    }

    public void setSchedulerName(String schedulerName) {
        this.schedulerName = schedulerName;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triggering that = (Triggering) o;

        if (taskName != null ? !taskName.equals(that.taskName) : that.taskName != null) return false;
        if (taskGroup != null ? !taskGroup.equals(that.taskGroup) : that.taskGroup != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        return shardingIndex != null ? shardingIndex.equals(that.shardingIndex) : that.shardingIndex == null;
    }

    @Override
    public int hashCode() {
        int result = taskName != null ? taskName.hashCode() : 0;
        result = 31 * result + (taskGroup != null ? taskGroup.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (shardingIndex != null ? shardingIndex.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "/" + taskGroup + "/" + taskName + "/" + time + "/" + shardingIndex;
    }
}
