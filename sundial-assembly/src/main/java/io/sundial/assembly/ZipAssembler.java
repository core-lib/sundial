package io.sundial.assembly;

import io.sundial.util.ResKit;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Stack;

/**
 * ZIP格式
 *
 * @author Payne 646742615@qq.com
 * 2018/12/28 20:19
 */
public class ZipAssembler implements Assembler {

    @Override
    public String suffix() {
        return "zip";
    }

    @Override
    public void assemble(File source, File target) throws IOException {
        FileOutputStream fileOutputStream = null;
        ZipArchiveOutputStream zipArchiveOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(target);
            zipArchiveOutputStream = new ZipArchiveOutputStream(fileOutputStream);
            Stack<String> parents = new Stack<>();
            Stack<File> files = new Stack<>();

            if (source.exists()) {
                parents.push(null);
                files.push(source);
            }

            while (!files.isEmpty()) {
                File file = files.pop();
                String parent = parents.pop();

                if (file.isDirectory()) {
                    ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(parent == null ? file.getName() + "/" : parent + "/" + file.getName() + "/");
                    zipArchiveEntry.setTime(file.lastModified());
                    zipArchiveOutputStream.putArchiveEntry(zipArchiveEntry);
                    zipArchiveOutputStream.closeArchiveEntry();
                    File[] children = file.listFiles();
                    for (int i = 0; children != null && i < children.length; i++) {
                        File child = children[i];
                        parents.push(parent == null ? file.getName() : parent + "/" + file.getName());
                        files.push(child);
                    }
                } else {
                    FileInputStream fileInputStream = null;
                    try {
                        fileInputStream = new FileInputStream(file);
                        ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(file, parent == null ? file.getName() : parent + "/" + file.getName());
                        zipArchiveOutputStream.putArchiveEntry(zipArchiveEntry);
                        ResKit.transfer(fileInputStream, zipArchiveOutputStream);
                        zipArchiveOutputStream.closeArchiveEntry();
                    } finally {
                        ResKit.close(fileInputStream);
                    }
                }
            }
        } finally {
            ResKit.close(zipArchiveOutputStream);
            ResKit.close(fileOutputStream);
        }
    }

    @Override
    public void disassemble(File source, File target) throws IOException {
        FileInputStream fileInputStream = null;
        ZipArchiveInputStream zipArchiveInputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            zipArchiveInputStream = new ZipArchiveInputStream(fileInputStream);
            ZipArchiveEntry zipArchiveEntry;
            while ((zipArchiveEntry = zipArchiveInputStream.getNextZipEntry()) != null) {
                if (zipArchiveEntry.isDirectory()) {
                    File parent = new File(target, zipArchiveEntry.getName());
                    if (!parent.exists() && !parent.mkdirs() && !parent.exists()) {
                        throw new IOException("could not make directory: " + parent);
                    }
                    continue;
                }
                FileOutputStream fileOutputStream = null;
                try {
                    File file = new File(target, zipArchiveEntry.getName());
                    File parent = file.getParentFile();
                    if (!parent.exists() && !parent.mkdirs() && !parent.exists()) {
                        throw new IOException("could not make directory: " + parent + " for zip archive entry: " + zipArchiveEntry);
                    }
                    fileOutputStream = new FileOutputStream(file);
                    ResKit.transfer(zipArchiveInputStream, fileOutputStream);
                } finally {
                    ResKit.close(fileOutputStream);
                }
            }
        } finally {
            ResKit.close(zipArchiveInputStream);
            ResKit.close(fileInputStream);
        }
    }
}
