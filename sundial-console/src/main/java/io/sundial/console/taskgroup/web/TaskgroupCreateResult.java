package io.sundial.console.taskgroup.web;

import io.sundial.console.ApiResult;

/**
 * 任务组创建结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 16:46
 */
public class TaskgroupCreateResult extends ApiResult {
}
