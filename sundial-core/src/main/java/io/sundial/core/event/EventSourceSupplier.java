package io.sundial.core.event;

import io.sundial.core.context.Context;
import io.sundial.core.context.Supplier;

/**
 * 事件源提供器
 */
public class EventSourceSupplier implements Supplier<EventSource> {

    @Override
    public EventSource supply(Context context) {
        return new EventSupport();
    }
}