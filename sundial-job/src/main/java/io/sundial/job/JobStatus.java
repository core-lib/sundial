package io.sundial.job;

/**
 * 执行状态
 */
public enum JobStatus {
    /**
     * 已提交
     */
    SUBMITTED(false),
    /**
     * 已拒绝
     */
    REJECTED(true),
    /**
     * 已取消
     */
    CANCELED(true),
    /**
     * 执行中
     */
    EXECUTING(false),
    /**
     * 已完成
     */
    COMPLETED(true),
    /**
     * 已失败
     */
    FAILED(true);

    /**
     * 不可改变的，即最终状态。
     */
    public final boolean immutable;

    JobStatus(boolean immutable) {
        this.immutable = immutable;
    }
}