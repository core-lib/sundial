package io.sundial.job;

import java.io.Serializable;

/**
 * 作业分片
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 13:22
 */
public class JobSharding implements Serializable {
    private static final long serialVersionUID = 7264196689845256462L;

    private String jobName;
    private String jobGroup;
    private int index;
    private int total;
    private String value;

    protected JobSharding() {

    }

    public JobSharding(String jobName, String jobGroup, int index, int total, String value) {
        this.jobName = jobName;
        this.jobGroup = jobGroup;
        this.index = index;
        this.total = total;
        this.value = value;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobSharding that = (JobSharding) o;

        if (index != that.index) return false;
        if (jobName != null ? !jobName.equals(that.jobName) : that.jobName != null) return false;
        return jobGroup != null ? jobGroup.equals(that.jobGroup) : that.jobGroup == null;
    }

    @Override
    public int hashCode() {
        int result = jobName != null ? jobName.hashCode() : 0;
        result = 31 * result + (jobGroup != null ? jobGroup.hashCode() : 0);
        result = 31 * result + index;
        return result;
    }

    @Override
    public String toString() {
        return "/" + jobGroup + "/" + jobName + "/" + index;
    }
}
