package io.sundial.job;

/**
 * 作业索引
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 17:38
 */
public class JobKey {
    private final String jobName;
    private final String jobGroup;

    public JobKey(String jobName) {
        this(jobName, null);
    }

    public JobKey(String jobName, String jobGroup) {
        this.jobName = jobName;
        this.jobGroup = jobGroup;
    }

    public String getJobName() {
        return jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobKey jobKey = (JobKey) o;

        if (jobName != null ? !jobName.equals(jobKey.jobName) : jobKey.jobName != null) return false;
        return jobGroup != null ? jobGroup.equals(jobKey.jobGroup) : jobKey.jobGroup == null;
    }

    @Override
    public int hashCode() {
        int result = jobName != null ? jobName.hashCode() : 0;
        result = 31 * result + (jobGroup != null ? jobGroup.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "" + jobGroup + "/" + jobName;
    }
}
