package io.sundial.assembly;

import io.sundial.util.ResKit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * GZIP格式
 *
 * @author Payne 646742615@qq.com
 * 2018/12/28 20:22
 */
public class GzipAssembler implements Assembler {

    @Override
    public String suffix() {
        return "gz";
    }

    @Override
    public void assemble(File source, File target) throws IOException {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        GZIPOutputStream gzipOutputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            gzipOutputStream = new GZIPOutputStream(fileOutputStream);
            ResKit.transfer(fileInputStream, gzipOutputStream);
        } finally {
            ResKit.close(gzipOutputStream);
            ResKit.close(fileOutputStream);
            ResKit.close(fileInputStream);
        }
    }

    @Override
    public void disassemble(File source, File target) throws IOException {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        GZIPInputStream gzipInputStream = null;
        try {
            fileInputStream = new FileInputStream(source);
            fileOutputStream = new FileOutputStream(target);
            gzipInputStream = new GZIPInputStream(fileInputStream);
            ResKit.transfer(gzipInputStream, fileOutputStream);
        } finally {
            ResKit.close(gzipInputStream);
            ResKit.close(fileOutputStream);
            ResKit.close(fileInputStream);
        }
    }
}
