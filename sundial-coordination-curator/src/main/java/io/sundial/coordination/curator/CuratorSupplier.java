package io.sundial.coordination.curator;

import io.sundial.core.context.Context;
import io.sundial.core.context.Supplier;
import io.sundial.core.context.exception.RoleNotSuppliedException;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * Curator提供者
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 10:36
 */
public class CuratorSupplier implements Supplier<CuratorFramework> {

    @Override
    public CuratorFramework supply(Context context) throws RoleNotSuppliedException {
        return CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5 * 1000)
                .retryPolicy(new ExponentialBackoffRetry(1000, 10, 10 * 1000))
                .namespace("sundial")
                .build();
    }
}
