package io.sundial.balance.exception;

import io.sundial.balance.BalancerException;

/**
 * 作业均衡异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 21:47
 */
public class BalancingException extends BalancerException {
    private static final long serialVersionUID = -6229486246693617657L;

    public BalancingException() {
    }

    public BalancingException(String message) {
        super(message);
    }

    public BalancingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BalancingException(Throwable cause) {
        super(cause);
    }
}
