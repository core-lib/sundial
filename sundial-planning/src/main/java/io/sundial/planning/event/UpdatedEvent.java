package io.sundial.planning.event;

import io.sundial.planning.PlannerEvent;
import io.sundial.task.Task;

/**
 * 任务更新事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 14:37
 */
public class UpdatedEvent extends PlannerEvent {
    private final Task task;

    public UpdatedEvent(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }
}
