package io.sundial.console.taskgroup.core;

/**
 * 调度周期
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 16:29
 */
public enum Period {

    MONTHLY {
        @Override
        public String toCronExpression(String expression) {
            return "0 0 0 " + expression + " * ? *";
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression.split(" ")[3];
        }
    },

    WEEKLY {
        @Override
        public String toCronExpression(String expression) {
            return "0 0 0 ? * " + expression + " *";
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression.split(" ")[5];
        }
    },

    DAILY {
        @Override
        public String toCronExpression(String expression) {
            return "0 0 " + expression + " * * ? *";
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression.split(" ")[2];
        }
    },

    HOURLY {
        @Override
        public String toCronExpression(String expression) {
            return "0 0 */" + expression + " * * ? *";
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression.split(" ")[1];
        }
    },

    MINUTELY {
        @Override
        public String toCronExpression(String expression) {
            return "0 */" + expression + " * * * ? *";
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression.split(" ")[0];
        }
    },

    CRON {
        @Override
        public String toCronExpression(String expression) {
            return expression;
        }

        @Override
        public String toExpression(String cronExpression) {
            return cronExpression;
        }
    },

    MANUALLY {
        @Override
        public String toCronExpression(String expression) {
            return null;
        }

        @Override
        public String toExpression(String cronExpression) {
            return null;
        }
    };

    public abstract String toCronExpression(String expression);

    public abstract String toExpression(String cronExpression);

}
