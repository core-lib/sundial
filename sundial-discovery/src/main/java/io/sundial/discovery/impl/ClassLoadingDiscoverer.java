package io.sundial.discovery.impl;

import io.sundial.core.context.Context;
import io.sundial.core.lifecycle.exception.DestroyingException;
import io.sundial.core.lifecycle.exception.InitializingException;

/**
 * 类加载方式的作业发现者
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 19:28
 */
public abstract class ClassLoadingDiscoverer extends LRUCachingDiscoverer {
    private ClassLoader classLoader;

    public ClassLoadingDiscoverer() {
    }

    public ClassLoadingDiscoverer(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    @Override
    protected void initializing(Context context) throws InitializingException {
        super.initializing(context);
        if (classLoader == null) {
            classLoader = Thread.currentThread().getContextClassLoader();
        }
        if (classLoader == null) {
            classLoader = ClassLoader.getSystemClassLoader();
        }
        if (classLoader == null) {
            classLoader = this.getClass().getClassLoader();
        }
    }

    @Override
    protected void destroying() throws DestroyingException {
        super.destroying();

        classLoader = null;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
