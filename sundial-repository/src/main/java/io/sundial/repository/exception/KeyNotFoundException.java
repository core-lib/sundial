package io.sundial.repository.exception;

import io.sundial.repository.RepositoryException;

/**
 * 数据不存在异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/18 9:55
 */
public class KeyNotFoundException extends RepositoryException {
    private static final long serialVersionUID = 7491367077783054435L;

    public KeyNotFoundException() {
    }

    public KeyNotFoundException(String message) {
        super(message);
    }

    public KeyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeyNotFoundException(Throwable cause) {
        super(cause);
    }
}
