package io.sundial.scheduler;

import io.sundial.executor.ExecutorEvent;

/**
 * 调度器事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 10:36
 */
public class SchedulerEvent extends ExecutorEvent {
}
