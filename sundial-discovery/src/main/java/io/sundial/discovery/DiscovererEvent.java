package io.sundial.discovery;

import io.sundial.core.event.Event;

/**
 * 作业发现者事件
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 17:48
 */
public class DiscovererEvent extends Event {
}
