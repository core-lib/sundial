package io.sundial.coordination;

/**
 * 节点回调状态码
 *
 * @author Payne 646742615@qq.com
 * 2018/12/19 12:23
 */
public interface CallbackStatus {

    int CODE_OK = 0;

    int CODE_SYSTEM_ERROR = -1;
    int CODE_RUNTIME_INCONSISTENCY = -2;
    int CODE_DATA_INCONSISTENCY = -3;
    int CODE_CONNECTION_LOSS = -4;
    int CODE_MARSHALLING_ERROR = -5;
    int CODE_UNIMPLEMENTED = -6;
    int CODE_OPERATION_TIMEOUT = -7;
    int CODE_BAD_ARGUMENTS = -8;

    int CODE_API_ERROR = -100;
    int CODE_NO_NODE = -101;
    int CODE_NO_AUTH = -102;
    int CODE_BAD_VERSION = -103;
    int CODE_NO_CHILDREN_FOR_EPHEMERAL = -108;
    int CODE_NODE_EXISTS = -110;
    int CODE_NOT_EMPTY = -111;
    int CODE_SESSION_EXPIRED = -112;
    int CODE_INVALID_CALLBACK = -113;
    int CODE_INVALID_ACL = -114;
    int CODE_AUTH_FAILED = -115;
    int CODE_SESSION_MOVED = -118;
    int CODE_NOT_READONLY = -119;

    int CODE_UNKNOWN_ERROR = Integer.MIN_VALUE;
}
