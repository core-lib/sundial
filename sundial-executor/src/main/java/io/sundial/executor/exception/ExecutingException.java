package io.sundial.executor.exception;

import io.sundial.executor.ExecutorException;

/**
 * 执行异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 12:13
 */
public class ExecutingException extends ExecutorException {
    private static final long serialVersionUID = -3392792768503729757L;

    public ExecutingException() {
    }

    public ExecutingException(String message) {
        super(message);
    }

    public ExecutingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecutingException(Throwable cause) {
        super(cause);
    }
}
