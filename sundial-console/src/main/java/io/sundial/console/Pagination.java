package io.sundial.console;

import java.util.List;

/**
 * 数据分页
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 11:07
 */
public class Pagination<T> {
    private long total;
    private List<T> items;

    public Pagination() {
    }

    public Pagination(long total, List<T> items) {
        this.total = total;
        this.items = items;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
