package io.sundial.core;

/**
 * 框架运行时异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:15
 */
public class SundialRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1774792324957507744L;

    public SundialRuntimeException() {
    }

    public SundialRuntimeException(String message) {
        super(message);
    }

    public SundialRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SundialRuntimeException(Throwable cause) {
        super(cause);
    }
}
