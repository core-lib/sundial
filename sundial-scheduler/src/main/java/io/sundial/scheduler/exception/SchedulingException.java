package io.sundial.scheduler.exception;

import io.sundial.scheduler.SchedulerException;

/**
 * 调度异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 10:36
 */
public class SchedulingException extends SchedulerException {
    private static final long serialVersionUID = -5629004216823590156L;

    public SchedulingException() {
    }

    public SchedulingException(String message) {
        super(message);
    }

    public SchedulingException(String message, Throwable cause) {
        super(message, cause);
    }

    public SchedulingException(Throwable cause) {
        super(cause);
    }
}
