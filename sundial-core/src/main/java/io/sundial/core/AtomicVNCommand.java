package io.sundial.core;


/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:07
 */
public interface AtomicVNCommand extends AtomicCommand {

    void run();

}
