package io.sundial.balance;

import io.sundial.core.SundialException;

/**
 * 均衡器异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 21:47
 */
public class BalancerException extends SundialException {
    private static final long serialVersionUID = -7197287393866888988L;

    public BalancerException() {
    }

    public BalancerException(String message) {
        super(message);
    }

    public BalancerException(String message, Throwable cause) {
        super(message, cause);
    }

    public BalancerException(Throwable cause) {
        super(cause);
    }
}
