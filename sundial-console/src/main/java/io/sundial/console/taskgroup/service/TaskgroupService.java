package io.sundial.console.taskgroup.service;

import io.sundial.console.Pagination;
import io.sundial.console.taskgroup.core.Taskgroup;

/**
 * 任务组服务接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 16:41
 */
public interface TaskgroupService {

    Taskgroup find(String name);

    void save(Taskgroup taskgroup);

    Pagination<Taskgroup> search(int pageNo, int pageSize, String keyword);

    void remove(String name);

    void update(String name, Taskgroup taskgroup);

}
