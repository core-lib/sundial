package io.sundial.engine;

import io.sundial.core.event.Event;

/**
 * 引擎事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 15:56
 */
public abstract class EngineEvent extends Event {
}
