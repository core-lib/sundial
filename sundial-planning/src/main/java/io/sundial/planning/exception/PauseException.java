package io.sundial.planning.exception;

import io.sundial.planning.PlannerException;

/**
 * 暂停异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 17:55
 */
public class PauseException extends PlannerException {
    private static final long serialVersionUID = 7131343947119208074L;

    public PauseException() {
    }

    public PauseException(String message) {
        super(message);
    }

    public PauseException(String message, Throwable cause) {
        super(message, cause);
    }

    public PauseException(Throwable cause) {
        super(cause);
    }
}
