package io.sundial.job.bind.impl;

import io.sundial.job.JobContext;
import io.sundial.job.JobParameter;
import io.sundial.job.bind.JobData;
import io.sundial.job.bind.JobField;
import io.sundial.job.bind.JobResolver;
import io.sundial.util.StrKit;

/**
 * 类型 转换器
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 21:44
 */
public class ClassResolver implements JobResolver {

    @Override
    public boolean supports(JobField field) {
        return field.type == Class.class;
    }

    @Override
    public JobParameter resolve(JobField field) {
        String value = field.value != null ? ((Class<?>) field.value).getName() : null;
        return new JobParameter(field.name)
                .setEditor("text")
                .setValue(value);
    }

    @Override
    public Object resolve(JobField field, JobData data, JobContext context) throws Exception {
        String value = data.get(field.name);
        return StrKit.isEmpty(value) ? DEFAULT : Class.forName(value);
    }
}
