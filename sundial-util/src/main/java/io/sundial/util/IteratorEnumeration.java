package io.sundial.util;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * 包装迭代器的枚举器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 21:35
 */
public class IteratorEnumeration<E> implements Enumeration<E> {
    private final Iterator<E> iterator;

    public IteratorEnumeration(Iterator<E> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public E nextElement() {
        return iterator.next();
    }
}
