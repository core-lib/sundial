package io.sundial.job;

/**
 * 作业执行取消回调
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 14:21
 */
public interface JobCancellation {

    /**
     * 当作业被成功取消时回调该方法
     *
     * @param jobCommand 作业命令
     */
    void onCanceled(JobCommand jobCommand);

}
