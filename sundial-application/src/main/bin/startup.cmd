@echo off

SET SUNDIAL_HOME=%~dp0%..

SET CONF_DIR=%SUNDIAL_HOME%\conf

SET LIB_DIR=%SUNDIAL_HOME%\lib

SET PLUGIN_DIR=%SUNDIAL_HOME%\plugin

SET CLASSPATH="%CONF_DIR%;%LIB_DIR%\*;%PLUGIN_DIR%\*"

SET SUNDIAL_MAIN=io.sundial.application.impl.StandaloneApplication

start java -Dsundial.home="%SUNDIAL_HOME%" -cp %CLASSPATH% %SUNDIAL_MAIN%