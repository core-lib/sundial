package io.sundial.core.context.exception;

import io.sundial.core.context.RoleException;

/**
 * 角色未提供异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:25
 */
public class RoleNotSuppliedException extends RoleException {
    private static final long serialVersionUID = -1675372063840928000L;

    public RoleNotSuppliedException() {
    }

    public RoleNotSuppliedException(String message) {
        super(message);
    }

    public RoleNotSuppliedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleNotSuppliedException(Throwable cause) {
        super(cause);
    }
}
