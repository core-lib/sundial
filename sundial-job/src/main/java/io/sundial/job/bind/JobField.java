package io.sundial.job.bind;

import io.sundial.job.Job;

import java.lang.reflect.Method;

/**
 * 作业形式参数
 */
public class JobField {
    public final Class<? extends Job> jobClass;
    public final Class<?> type;
    public final String name;
    public final Object value;
    public final Method getter;
    public final Method setter;

    public JobField(Class<? extends Job> jobClass, Class<?> type, String name, Object value, Method getter, Method setter) {
        this.jobClass = jobClass;
        this.type = type;
        this.name = name;
        this.value = value;
        this.getter = getter;
        this.setter = setter;
    }
}