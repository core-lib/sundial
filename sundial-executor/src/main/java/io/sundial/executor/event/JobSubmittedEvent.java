package io.sundial.executor.event;

import io.sundial.job.JobCommand;

/**
 * 作业命令已提交事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:11
 */
public class JobSubmittedEvent extends JobEvent {

    public JobSubmittedEvent(JobCommand jobCommand) {
        super(jobCommand);
    }
}
