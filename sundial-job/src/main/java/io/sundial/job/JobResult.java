package io.sundial.job;

import java.io.Serializable;

/**
 * 作业结果
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 12:06
 */
public class JobResult implements Serializable {
    private static final long serialVersionUID = -2767658619494563295L;

    /**
     * 作业执行成功标记
     * 只有{@code 0}代表作业执行成功，除了{@code 0}其他标记都认为是失败。
     */
    public static final int SUCCESS = 0;
    public static final JobResult OK = ok("OK");

    private int code = SUCCESS;
    private String message;

    public JobResult() {
    }

    public JobResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static JobResult ok() {
        return OK;
    }

    public static JobResult ok(String message) {
        return valueOf(SUCCESS, message);
    }

    public static JobResult valueOf(int code, String message) {
        return new JobResult(code, message);
    }

    /**
     * 作业执行结果标记
     * 判断作业是否成功执行的唯一判断
     *
     * @return 作业执行结果标记
     */
    public int getCode() {
        return code;
    }

    /**
     * 作业执行结果消息
     * 作业执行结果的一个友好消息，不推荐通过该返回值判断作业的执行结果。
     *
     * @return 作业执行结果消息
     */
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "" + code + " " + message;
    }
}
