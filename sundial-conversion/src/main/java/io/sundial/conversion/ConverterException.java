package io.sundial.conversion;

import io.sundial.core.SundialException;

/**
 * 转换器异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:28
 */
public class ConverterException extends SundialException {
    private static final long serialVersionUID = -7880662202092875835L;

    public ConverterException() {
    }

    public ConverterException(String message) {
        super(message);
    }

    public ConverterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConverterException(Throwable cause) {
        super(cause);
    }
}
