package io.sundial.engine.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 带日志的引擎
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 19:49
 */
public abstract class LoggingEngine extends StatefulEngine {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}
