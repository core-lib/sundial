package io.sundial.coordination.tree;

import io.sundial.coordination.node.Node;

import java.util.Map;

/**
 * 节点树
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 11:32
 */
public interface Tree {

    /**
     * 获取根节点路径
     *
     * @return 根节点路径
     */
    String getRoot();

    /**
     * 获取指定父节点的所有子节点，不包括递归子节点。
     *
     * @param path 父节点路径
     * @return 指定父节点的所有子节点
     */
    Map<String, Node> getChildren(String path);

    /**
     * 获取指定的节点
     *
     * @param path 节点路径
     * @return 指定节点
     */
    Node getChild(String path);

}
