package io.sundial.console.datasource.web;

import io.sundial.console.ApiResult;

/**
 * 数据源更新结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 13:26
 */
public class DatasourceUpdateResult extends ApiResult {
}
