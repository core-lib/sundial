package io.sundial.console.datasource.web;

/**
 * 数据源状态切换参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 12:53
 */
public class DatasourceStatusParam {
    /**
     * 是否设为禁用
     */
    private boolean disabled;

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
