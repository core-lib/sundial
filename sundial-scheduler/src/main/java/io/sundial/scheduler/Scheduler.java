package io.sundial.scheduler;

import io.sundial.executor.Executor;
import io.sundial.scheduler.exception.SchedulingException;
import io.sundial.task.Task;

/**
 * 调度器接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 10:30
 */
public interface Scheduler extends Executor {

    /**
     * 立即调度一个任务
     *
     * @param task 任务
     * @throws SchedulingException 任务调度异常
     */
    void schedule(Task task) throws SchedulingException;

    /**
     * 立刻调度一个预期时间点的任务
     *
     * @param task 任务
     * @param time 预期时间点
     * @throws SchedulingException 任务调度异常
     */
    void schedule(Task task, long time) throws SchedulingException;

}
