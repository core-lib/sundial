package io.sundial.hibernate;


import io.sundial.core.AtomicCommand;
import org.hibernate.Session;

/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:06
 */
public interface TrxVTCall<E extends Exception> extends AtomicCommand {

    void call(Session sex) throws E;

}
