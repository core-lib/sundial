package io.sundial.coordination.watching;

import io.sundial.coordination.CoordinatorException;

/**
 * 观察者句柄
 *
 * @author Payne 646742615@qq.com
 * 2018/12/28 16:12
 */
public interface EventWatching {

    /**
     * 获取观察者句柄状态
     *
     * @return 观察者句柄状态
     */
    State state();

    /**
     * 启动观察
     *
     * @throws CoordinatorException 协调者异常
     */
    void start() throws CoordinatorException;

    /**
     * 暂停观察
     *
     * @throws CoordinatorException 协调者异常
     */
    void pause() throws CoordinatorException;

    /**
     * 关闭观察，无法再开启！
     *
     * @throws CoordinatorException 协调者异常
     */
    void close() throws CoordinatorException;

    /**
     * 观察者句柄状态
     */
    enum State {
        /**
         * 观察中
         */
        WATCHING,
        /**
         * 已暂停
         */
        PAUSED,
        /**
         * 已关闭
         */
        CLOSED
    }

}
