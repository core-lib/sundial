package io.sundial.console.task.dto;

import io.sundial.console.task.web.TaskChild;
import io.sundial.console.taskgroup.core.Period;
import io.sundial.task.Task;
import org.quartz.CronExpression;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.text.ParseException;
import java.util.*;

/**
 * 任务VO
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 13:23
 */
public class TaskVO {
    /**
     * 任务名称
     */
    @NotEmpty(message = "任务名称不能为空")
    @Pattern(regexp = "[^/;\\\\]+", message = "任务名称不能包含[/;\\]")
    private String name;
    /**
     * 任务分组
     */
    @NotEmpty(message = "任务分组不能为空")
    @Pattern(regexp = "[^/;\\\\]+", message = "任务分组不能包含[/;\\]")
    private String group;
    /**
     * 作业名称
     */
    @NotEmpty(message = "作业名称不能为空")
    private String jobName;
    /**
     * 作业分组
     */
    @NotEmpty(message = "作业所属组不能为空")
    private String jobGroup;
    /**
     * 任务描述
     */
    private String description;
    /**
     * 任务执行周期类型
     */
    private Period period;
    /**
     * 任务执行周期表达式
     */
    private String expression;
    /**
     * 任务优先权
     */
    private int priority;
    /**
     * 任务开启时间
     */
    private Date dateStart;
    /**
     * 任务结束时间
     */
    private Date dateEnd;
    /**
     * 作业参数
     */
    private Map<String, String> jobArguments;
    /**
     * 子任务
     */
    private List<TaskChild> children;

    /**
     * 上次调度时间
     */
    private Date prevTime;

    /**
     * 下次调度时间
     */
    private Date nextTime;

    public TaskVO() {
    }

    public TaskVO(Task task) {
        this.name = task.getName();
        this.group = task.getGroup();
        this.jobName = task.getJobName();
        this.jobGroup = task.getJobGroup();
        this.description = task.getDescription();
        this.period = Period.CRON;
        this.expression = task.getCronExpression();
        this.priority = task.getPriority();
        this.dateStart = task.getDateStart();
        this.dateEnd = task.getDateEnd();
        this.jobArguments = task.getJobArguments();
        this.children = new ArrayList<>();
        Set<Task.Key> children = task.getChildren();
        if (children != null) {
            for (Task.Key child : children) {
                this.children.add(new TaskChild(child.getGroup(), child.getName()));
            }
        }

        try {
            CronExpression cronExpression = new CronExpression(expression);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Map<String, String> getJobArguments() {
        return jobArguments;
    }

    public void setJobArguments(Map<String, String> jobArguments) {
        this.jobArguments = jobArguments;
    }

    public List<TaskChild> getChildren() {
        return children;
    }

    public void setChildren(List<TaskChild> children) {
        this.children = children;
    }

}
