package io.sundial.core;

/**
 * 回调接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:58
 */
public interface Callback<R, E extends Exception> {

    /**
     * 回调函数
     *
     * @param success   是否成功
     * @param result    结果
     * @param exception 异常，有可能为{@code null}，即便{@code success == false}也仍有可能为{@code null}
     */
    void call(boolean success, R result, E exception) throws Exception;

}
