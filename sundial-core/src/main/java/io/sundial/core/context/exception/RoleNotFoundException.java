package io.sundial.core.context.exception;

import io.sundial.core.context.RoleException;

/**
 * 角色不存在异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:19
 */
public class RoleNotFoundException extends RoleException {
    private static final long serialVersionUID = -560880651713636935L;

    public RoleNotFoundException() {
    }

    public RoleNotFoundException(String message) {
        super(message);
    }

    public RoleNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleNotFoundException(Throwable cause) {
        super(cause);
    }
}
