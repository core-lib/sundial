package io.sundial.console.task.core;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 触发记录
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 16:27
 */
@Entity
@Table(name = "sundial_triggering")
public class Logging implements Serializable {
    private static final long serialVersionUID = 3274765626429770352L;

    @Id
    @EmbeddedId
    @AttributeOverrides({
            @AttributeOverride(name = "taskName", column = @Column(name = "task_name")),
            @AttributeOverride(name = "taskGroup", column = @Column(name = "task_group")),
            @AttributeOverride(name = "time", column = @Column(name = "scheduling_time")),
            @AttributeOverride(name = "shardingIndex", column = @Column(name = "sharding_index"))
    })
    private Key key;

    @Column(name = "scheduler_name")
    private String schedulerName;

    @Column(name = "executor_name")
    private String executorName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "code")
    private Integer code;

    @Column(name = "message")
    @Lob
    private String message;

    @Column(name = "result")
    @Lob
    private String result;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created")
    private Date dateCreated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated")
    private Date lastUpdated;

    @Version
    @Column(name = "version")
    private Integer version;

    @Embeddable
    public static class Key implements Serializable {
        private static final long serialVersionUID = -3763847321056923484L;

        private String taskName;
        private String taskGroup;
        private Long time;
        private Integer shardingIndex;

        public Key() {
        }

        public Key(String taskName, String taskGroup, Long time, Integer shardingIndex) {
            this.taskName = taskName;
            this.taskGroup = taskGroup;
            this.time = time;
            this.shardingIndex = shardingIndex;
        }

        public String getTaskName() {
            return taskName;
        }

        public String getTaskGroup() {
            return taskGroup;
        }

        public Long getTime() {
            return time;
        }

        public Integer getShardingIndex() {
            return shardingIndex;
        }

        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }

        public void setTaskGroup(String taskGroup) {
            this.taskGroup = taskGroup;
        }

        public void setTime(Long time) {
            this.time = time;
        }

        public void setShardingIndex(Integer shardingIndex) {
            this.shardingIndex = shardingIndex;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (taskName != null ? !taskName.equals(key.taskName) : key.taskName != null) return false;
            if (taskGroup != null ? !taskGroup.equals(key.taskGroup) : key.taskGroup != null) return false;
            if (time != null ? !time.equals(key.time) : key.time != null) return false;
            return shardingIndex != null ? shardingIndex.equals(key.shardingIndex) : key.shardingIndex == null;
        }

        @Override
        public int hashCode() {
            int result = taskName != null ? taskName.hashCode() : 0;
            result = 31 * result + (taskGroup != null ? taskGroup.hashCode() : 0);
            result = 31 * result + (time != null ? time.hashCode() : 0);
            result = 31 * result + (shardingIndex != null ? shardingIndex.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "/" + taskGroup + "/" + taskName + "/" + time + "/" + shardingIndex;
        }
    }

    public enum Status {
        /**
         * 已提交
         */
        SUBMITTED,
        /**
         * 已拒绝
         */
        REJECTED,
        /**
         * 已取消
         */
        CANCELED,
        /**
         * 执行中
         */
        EXECUTING,
        /**
         * 已完成
         */
        COMPLETED,
        /**
         * 已失败
         */
        FAILED,
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getSchedulerName() {
        return schedulerName;
    }

    public void setSchedulerName(String schedulerName) {
        this.schedulerName = schedulerName;
    }

    public String getExecutorName() {
        return executorName;
    }

    public void setExecutorName(String executorName) {
        this.executorName = executorName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Logging that = (Logging) o;

        return key != null ? key.equals(that.key) : that.key == null;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public String toString() {
        return key != null ? key.toString() : null;
    }
}
