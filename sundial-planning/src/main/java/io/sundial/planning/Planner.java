package io.sundial.planning;

import io.sundial.core.event.EventListenable;
import io.sundial.core.lifecycle.Lifecycle;
import io.sundial.planning.exception.ClearException;
import io.sundial.planning.exception.CloseException;
import io.sundial.planning.exception.PauseException;
import io.sundial.planning.exception.StartException;
import io.sundial.repository.Repository;
import io.sundial.task.Task;

/**
 * 任务规划者
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 13:43
 */
public interface Planner extends Lifecycle, Repository<Task.Key, Task>, EventListenable<PlannerEvent> {

    /**
     * 开始规划
     */
    void start() throws StartException;

    /**
     * 暂停规划
     */
    void pause() throws PauseException;

    /**
     * 清除状态
     */
    void clear() throws ClearException;

    /**
     * 关闭规划
     */
    void close() throws CloseException;


}
