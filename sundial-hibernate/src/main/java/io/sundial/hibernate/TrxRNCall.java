package io.sundial.hibernate;


import org.hibernate.Session;

/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:08
 */
public interface TrxRNCall<R> {

    R call(Session sex);

}
