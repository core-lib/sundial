package io.sundial.job.anno;

import java.lang.annotation.*;

/**
 * 参数选项注解
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 21:48
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
@Documented
public @interface Option {

    /**
     * 选项值
     *
     * @return 选项值
     */
    String value();

    /**
     * 选项文案，缺省情况下选项值即文案。
     *
     * @return 选项文案
     */
    String label() default "";

}
