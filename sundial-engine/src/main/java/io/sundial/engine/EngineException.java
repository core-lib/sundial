package io.sundial.engine;

import io.sundial.core.SundialException;

/**
 * 引擎异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 14:12
 */
public abstract class EngineException extends SundialException {
    private static final long serialVersionUID = -3451808535948053472L;

    public EngineException() {
    }

    public EngineException(String message) {
        super(message);
    }

    public EngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public EngineException(Throwable cause) {
        super(cause);
    }
}
