package io.sundial.balance;

import io.sundial.balance.exception.BalancingException;
import io.sundial.executor.Executor;
import io.sundial.job.JobSharding;
import io.sundial.task.Task;

import java.util.List;
import java.util.Map;

/**
 * 作业分片均衡器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 11:00
 */
public interface Balancer {

    /**
     * 均衡算法名称
     *
     * @return 均衡算法名称
     */
    String getAlgorithm();

    /**
     * 均衡分配
     *
     * @param executors 当前可用的执行器列表
     * @param task      任务
     * @return 分配结果
     */
    Map<String, List<JobSharding>> balance(Map<String, Executor.Information> executors, Task task) throws BalancingException;

}
