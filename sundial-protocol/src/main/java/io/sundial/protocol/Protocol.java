package io.sundial.protocol;

import io.sundial.protocol.exception.MarshallingException;
import io.sundial.protocol.exception.UnmarshallingException;

/**
 * 协议
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:37
 */
public interface Protocol {

    /**
     * 换行
     */
    byte[] CRLF = new byte[]{'\r', '\n'};

    /**
     * 获取协议名称
     *
     * @return 协议名称
     */
    String getName();

    /**
     * 获取协议版本
     *
     * @return 协议版本
     */
    String getVersion();

    /**
     * 编组
     *
     * @param obj 对象
     * @return 编队后数据
     * @throws MarshallingException 编组异常
     */
    byte[] marshall(Object obj) throws MarshallingException;

    /**
     * 解组
     *
     * @param data 编组后数据
     * @param type 对象类型
     * @param <T>  对象类型变量
     * @return 解组后对象
     * @throws UnmarshallingException 解组异常
     */
    <T> T unmarshal(byte[] data, Class<T> type) throws UnmarshallingException;

}
