package io.sundial.conversion.exception;

import io.sundial.conversion.ConverterException;

/**
 * 序列化异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:30
 */
public class SerializeException extends ConverterException {
    private static final long serialVersionUID = -7764567733603287092L;

    public SerializeException() {
    }

    public SerializeException(String message) {
        super(message);
    }

    public SerializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SerializeException(Throwable cause) {
        super(cause);
    }
}
