package io.sundial.coordination.node;

/**
 * 节点监听器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 16:31
 */
public interface NodeWatcher {

    /**
     * 当观察到节点变化时回调
     *
     * @param event 节点变化事件
     */
    void onWatched(NodeEvent event) throws Exception;

}
