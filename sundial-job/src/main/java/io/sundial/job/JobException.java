package io.sundial.job;

import io.sundial.core.SundialException;

/**
 * 作业异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 12:07
 */
public class JobException extends SundialException {
    private static final long serialVersionUID = -2169719382958055309L;

    public JobException() {
    }

    public JobException(String message) {
        super(message);
    }

    public JobException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobException(Throwable cause) {
        super(cause);
    }
}
