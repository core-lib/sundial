package io.sundial.coordination.curator;

import com.google.common.base.Preconditions;
import io.sundial.coordination.node.Node;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.NodeCache;

/**
 * Curator节点
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 12:05
 */
public class CachedNode implements Node {
    private final String path;
    private final NodeCache cache;

    CachedNode(String path, NodeCache cache) {
        Preconditions.checkNotNull(path, "path must not be null");
        Preconditions.checkNotNull(cache, "cache must not be null");
        this.path = path;
        this.cache = cache;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public byte[] getData() {
        ChildData data = cache.getCurrentData();
        return data != null ? data.getData() : null;
    }

    @Override
    public int getVersion() {
        ChildData data = cache.getCurrentData();
        return data != null ? data.getStat().getVersion() : -1;
    }
}
