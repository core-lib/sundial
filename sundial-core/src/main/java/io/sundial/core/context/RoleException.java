package io.sundial.core.context;

import io.sundial.core.SundialRuntimeException;

/**
 * 角色异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:10
 */
public class RoleException extends SundialRuntimeException {
    private static final long serialVersionUID = 8695872880220292580L;

    public RoleException() {
    }

    public RoleException(String message) {
        super(message);
    }

    public RoleException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleException(Throwable cause) {
        super(cause);
    }
}
