package io.sundial.console.taskgroup.web;

import javax.validation.constraints.Min;

/**
 * 数据源分页搜索参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 10:39
 */
public class TaskgroupSearchParam {

    /**
     * 页码，从1开始
     */
    @Min(value = 1, message = "页码不能小于1")
    private int pageNo;

    /**
     * 每页最多显示条目，不能小于1
     */
    @Min(value = 1, message = "每页最多显示条数不能小于1")
    private int pageSize;

    /**
     * 查询关键字，可以不填。
     */
    private String keyword;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
