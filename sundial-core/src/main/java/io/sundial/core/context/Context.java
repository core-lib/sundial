package io.sundial.core.context;

import java.util.Map;

/**
 * 应用上下文
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:45
 */
public interface Context extends Iterable<String> {

    <T> T get(String name) throws RoleException, ClassCastException;

    <T> T get(String name, Supplier<T> supplier) throws RoleException, ClassCastException;

    <T> T get(Class<T> type) throws RoleException;

    <T> T get(Class<T> type, Supplier<T> supplier) throws RoleException;

    <T> T get(Clazz<T> type, Supplier<T> supplier) throws RoleException;

    Class<?> check(String name) throws RoleException;

    <T> Map<String, T> fetch(Class<T> type) throws RoleException;

}
