package io.sundial.job;

import java.io.Serializable;
import java.util.Map;

/**
 * 作业定义
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 20:45
 */
public class JobDefinition implements Comparable<JobDefinition>, Serializable {
    private static final long serialVersionUID = -4175343948436258919L;

    private String name;
    private String group;
    private String type;
    private String version;
    private int shardable;
    private Map<String, JobParameter> parameters;
    private String description;

    public JobDefinition() {
    }

    public JobDefinition(String name, String group, String type, String version, int shardable, Map<String, JobParameter> parameters, String description) {
        this.name = name;
        this.group = group;
        this.type = type;
        this.version = version;
        this.shardable = shardable;
        this.parameters = parameters;
        this.description = description;
    }

    @Override
    public int compareTo(JobDefinition that) {
        String a = this.version;
        String b = that.version;
        if (a == null || b == null) throw new NullPointerException();
        if (a.length() == b.length() && a.equals(b)) return 0;
        String[] as = a.split("\\.");
        String[] bs = b.split("\\.");
        for (int i = 0; i < as.length && i < bs.length; i++) {
            int A = Integer.valueOf(as[i]);
            int B = Integer.valueOf(bs[i]);
            int C = Integer.compare(A, B);
            if (C != 0) return C;
        }
        return Integer.compare(as.length, bs.length);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getShardable() {
        return shardable;
    }

    public void setShardable(int shardable) {
        this.shardable = shardable;
    }

    public Map<String, JobParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, JobParameter> parameters) {
        this.parameters = parameters;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobDefinition that = (JobDefinition) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return group != null ? group.equals(that.group) : that.group == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "" + group + "/" + name;
    }
}
