package io.sundial.core;

/**
 * 程序锁
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 11:38
 */
public class Locker {
    private final Object mutex = new Object();

    public static Locker create() {
        return new Locker();
    }

    public void acquire() {
        synchronized (mutex) {
            try {
                mutex.wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void release() {
        synchronized (mutex) {
            mutex.notifyAll();
        }
    }

}
