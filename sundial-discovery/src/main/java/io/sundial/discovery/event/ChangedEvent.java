package io.sundial.discovery.event;

import io.sundial.discovery.DiscovererEvent;

/**
 * 作业发现者变化事件
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 17:57
 */
public class ChangedEvent extends DiscovererEvent {
}
