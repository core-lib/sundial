package io.sundial.planning.exception;

import io.sundial.planning.PlannerException;

/**
 * 关闭异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 18:00
 */
public class CloseException extends PlannerException {
    private static final long serialVersionUID = 7192577327628458175L;

    public CloseException() {
    }

    public CloseException(String message) {
        super(message);
    }

    public CloseException(String message, Throwable cause) {
        super(message, cause);
    }

    public CloseException(Throwable cause) {
        super(cause);
    }
}
