package io.sundial.engine.impl;

import io.sundial.core.context.Context;
import io.sundial.core.event.*;
import io.sundial.core.lifecycle.exception.DestroyingException;
import io.sundial.core.lifecycle.exception.InitializingException;
import io.sundial.engine.EngineEvent;
import io.sundial.engine.event.DestroyedEvent;
import io.sundial.engine.event.InitializedEvent;
import io.sundial.engine.event.ShuttedEvent;
import io.sundial.engine.event.StartedEvent;
import io.sundial.engine.exception.ShuttingException;
import io.sundial.engine.exception.StartingException;

/**
 * 事件引擎
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 15:55
 */
public abstract class EventEngine extends NamedEngine implements EventListenable<EngineEvent> {
    private EventSource eventSource;

    protected void fire(Event event) {
        eventSource.fire(event);
    }

    protected void shut() {
        eventSource.shut();
    }

    @Override
    protected void initializing(Context context) throws InitializingException {
        super.initializing(context);

        //region 绑定事件源
        eventSource = eventSource != null
                ? eventSource
                : context.get(EventSource.class, new EventSourceSupplier());
        eventSource.fire(new InitializedEvent());
        //endregion
    }

    @Override
    protected void destroying() throws DestroyingException {
        super.destroying();

        eventSource.fire(new DestroyedEvent());
        eventSource = null;
    }

    @Override
    public void startup() throws StartingException {
        super.startup();
        eventSource.fire(new StartedEvent());
    }

    @Override
    public void shutdown() throws ShuttingException {
        super.shutdown();
        eventSource.fire(new ShuttedEvent());
    }

    @Override
    public void addEventListener(EventListener<? extends EngineEvent> listener) {
        eventSource.addEventListener(listener);
    }

    @Override
    public void removeEventListener(EventListener<? extends EngineEvent> listener) {
        eventSource.removeEventListener(listener);
    }

    public EventSource getEventSource() {
        return eventSource;
    }

    public void setEventSource(EventSource eventSource) {
        this.eventSource = eventSource;
    }
}
