package io.sundial.job.bind;

import io.sundial.core.SundialException;

/**
 * 作业参数不支持异常
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 17:35
 */
public class JobResolvingException extends SundialException {
    private static final long serialVersionUID = -2070979008962509472L;

    public JobResolvingException() {
    }

    public JobResolvingException(String message) {
        super(message);
    }

    public JobResolvingException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobResolvingException(Throwable cause) {
        super(cause);
    }
}