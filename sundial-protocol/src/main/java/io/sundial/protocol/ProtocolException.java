package io.sundial.protocol;

import io.sundial.core.SundialException;

/**
 * 协议异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:39
 */
public class ProtocolException extends SundialException {
    private static final long serialVersionUID = 5326061526794373067L;

    public ProtocolException() {
    }

    public ProtocolException(String message) {
        super(message);
    }

    public ProtocolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProtocolException(Throwable cause) {
        super(cause);
    }
}
