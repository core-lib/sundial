package io.sundial.console.task.service;

import io.sundial.console.Pagination;
import io.sundial.console.task.core.Logging;

import java.text.ParseException;

/**
 * 执行记录Service接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/14 13:34
 */
public interface LoggingService {

    Pagination<Logging> search(String taskGroup, String taskName, String dateStart, String dateEnd, Logging.Status status, int pageNo, int pageSize) throws ParseException;

}
