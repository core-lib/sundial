package io.sundial.executor;

import io.sundial.engine.EngineEvent;

/**
 * 执行器事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:06
 */
public abstract class ExecutorEvent extends EngineEvent {
}
