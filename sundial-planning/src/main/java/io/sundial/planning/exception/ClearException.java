package io.sundial.planning.exception;

import io.sundial.planning.PlannerException;

/**
 * 清除异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 17:56
 */
public class ClearException extends PlannerException {
    private static final long serialVersionUID = -4324979275612697197L;

    public ClearException() {
    }

    public ClearException(String message) {
        super(message);
    }

    public ClearException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClearException(Throwable cause) {
        super(cause);
    }
}
