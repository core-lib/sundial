package io.sundial.engine.exception;

import io.sundial.engine.EngineException;

/**
 * 引擎启动异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 14:13
 */
public class StartingException extends EngineException {
    private static final long serialVersionUID = 8489390005895570628L;

    public StartingException() {
    }

    public StartingException(String message) {
        super(message);
    }

    public StartingException(String message, Throwable cause) {
        super(message, cause);
    }

    public StartingException(Throwable cause) {
        super(cause);
    }
}
