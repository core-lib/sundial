package io.sundial.console.task.web;

import io.sundial.console.ApiResult;

/**
 * 作业新建结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 14:41
 */
public class TaskCreateResult extends ApiResult {
}
