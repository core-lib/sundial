package io.sundial.repository.exception;

import io.sundial.repository.RepositoryException;

/**
 * 数据冲突异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/18 9:54
 */
public class KeyConflictedException extends RepositoryException {
    private static final long serialVersionUID = 2288994882861637471L;

    public KeyConflictedException() {
    }

    public KeyConflictedException(String message) {
        super(message);
    }

    public KeyConflictedException(String message, Throwable cause) {
        super(message, cause);
    }

    public KeyConflictedException(Throwable cause) {
        super(cause);
    }
}
