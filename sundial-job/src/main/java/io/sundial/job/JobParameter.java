package io.sundial.job;

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.Map;

/**
 * 作业形式参数
 * 作业形式参数的定义主要用于在控制台界面上创建作业时自动生成参数设置的输入编辑器，为作业维护提供便利性。
 *
 * @author Payne 646742615@qq.com
 * 2018/12/6 13:44
 */
public class JobParameter implements Serializable {
    private static final long serialVersionUID = -7376113177446598693L;

    private String name;
    private String label;
    private String editor;
    private Map<String, String> options;
    private String value;
    private String description;

    protected JobParameter() {
    }

    public JobParameter(String name) {
        Preconditions.checkNotNull(name, "name must not be null");
        this.name = name;
        this.label = name;
    }

    /**
     * 参数名称
     *
     * @return 参数名称
     */
    public String getName() {
        return name;
    }

    public JobParameter setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 文案
     *
     * @return 文案
     */
    public String getLabel() {
        return label;
    }

    public JobParameter setLabel(String label) {
        this.label = label;
        return this;
    }

    /**
     * 参数编辑器
     *
     * @return 参数名称
     */
    public String getEditor() {
        return editor;
    }

    public JobParameter setEditor(String editor) {
        this.editor = editor;
        return this;
    }

    /**
     * 枚举类型参数的可选项
     *
     * @return 可选项
     */
    public Map<String, String> getOptions() {
        return options;
    }

    public JobParameter setOptions(Map<String, String> options) {
        this.options = options;
        return this;
    }

    /**
     * 缺省值
     *
     * @return 缺省值
     */
    public String getValue() {
        return value;
    }

    public JobParameter setValue(String value) {
        this.value = value;
        return this;
    }

    /**
     * 参数详细描述
     *
     * @return 参数详细描述
     */
    public String getDescription() {
        return description;
    }

    public JobParameter setDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobParameter that = (JobParameter) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "" + name;
    }
}
