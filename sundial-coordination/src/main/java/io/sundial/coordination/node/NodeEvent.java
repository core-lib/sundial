package io.sundial.coordination.node;

import com.google.common.base.Preconditions;

/**
 * 节点监听事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 10:58
 */
public class NodeEvent {
    private final String path;
    private final Node node;

    public NodeEvent(String path, Node node) {
        Preconditions.checkNotNull(path, "path must not be null");
        Preconditions.checkNotNull(node, "node must not be null");
        this.path = path;
        this.node = node;
    }

    public String getPath() {
        return path;
    }

    public Node getNode() {
        return node;
    }

}
