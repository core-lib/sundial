package io.sundial.core.event;

/**
 * 事件可监听的接口
 * 一个类实现该接口即表示可以接收事件监听器的监听，并保证由该事件源发生的事件都能触发对应监听器的回调，且所有事件都是声明的类型参数的本身或其子类。
 *
 * @author Payne 646742615@qq.com
 * 2018/12/17 15:49
 */
public interface EventListenable<E extends Event> {

    /**
     * 接受一个事件监听器
     * 通过监听器的类型指定的泛型参数类型来绑定监听事件类型，如果无法判别监听的事件类型则抛出{@link IllegalArgumentException}，
     * 如果传入{@code null}则抛出{@link NullPointerException}。
     *
     * @param listener 事件监听器
     */
    void addEventListener(EventListener<? extends E> listener);

    /**
     * 屏蔽一个事件监听器
     * 即移除一个事件监听器
     *
     * @param listener 事件监听器
     */
    void removeEventListener(EventListener<? extends E> listener);

}
