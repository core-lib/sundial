package io.sundial.core.lifecycle;

import io.sundial.core.SundialException;

/**
 * 生命周期异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:48
 */
public abstract class LifecycleException extends SundialException {
    private static final long serialVersionUID = 8903002415308934642L;

    public LifecycleException() {
    }

    public LifecycleException(String message) {
        super(message);
    }

    public LifecycleException(String message, Throwable cause) {
        super(message, cause);
    }

    public LifecycleException(Throwable cause) {
        super(cause);
    }
}
