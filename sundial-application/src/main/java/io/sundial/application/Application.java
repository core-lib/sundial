package io.sundial.application;

import io.sundial.scheduler.Scheduler;

/**
 * 应用接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 20:30
 */
public interface Application extends Scheduler {

}
