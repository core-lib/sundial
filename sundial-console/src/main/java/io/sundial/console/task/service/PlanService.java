package io.sundial.console.task.service;

import io.sundial.console.Pagination;
import io.sundial.console.task.core.Plan;

/**
 * 计划Service接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 12:48
 */
public interface PlanService {

    Pagination<Plan> search(String keyword, String group, int pageNo, int pageSize);

}
