package io.sundial.console.taskgroup.dao;

import io.sundial.console.taskgroup.core.Period;
import io.sundial.console.taskgroup.core.Taskgroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * 任务组仓储接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 16:41
 */
public interface TaskgroupRepository extends JpaRepository<Taskgroup, String> {

    @Query("FROM Taskgroup AS ds WHERE ds.name LIKE :keyword OR ds.description LIKE :keyword")
    Page<Taskgroup> search(@Param("keyword") String keyword, Pageable pageable);

    @Modifying
    @Query("UPDATE Taskgroup AS ds SET ds.period = :period, ds.expression = :expression, ds.description = :description, ds.lastUpdated = :lastUpdated, ds.version = ds.version + 1 WHERE ds.name = :name")
    void update(
            @Param("name") String name,
            @Param("period") Period period,
            @Param("expression") String expression,
            @Param("description") String description,
            @Param("lastUpdated") Date lastUpdated
    );

    @Modifying
    @Query("DELETE FROM Taskgroup WHERE name = :name")
    void delete(@Param("name") String name);

}
