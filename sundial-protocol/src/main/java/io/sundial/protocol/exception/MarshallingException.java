package io.sundial.protocol.exception;

import io.sundial.protocol.ProtocolException;

/**
 * 编码异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:43
 */
public class MarshallingException extends ProtocolException {
    private static final long serialVersionUID = -6921205718942979921L;

    public MarshallingException() {
    }

    public MarshallingException(String message) {
        super(message);
    }

    public MarshallingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MarshallingException(Throwable cause) {
        super(cause);
    }
}
