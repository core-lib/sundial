package io.sundial.conversion.exception;

import io.sundial.conversion.ConverterException;

/**
 * 反序列化异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:30
 */
public class DeserializeException extends ConverterException {
    private static final long serialVersionUID = 3367778571559718959L;

    public DeserializeException() {
    }

    public DeserializeException(String message) {
        super(message);
    }

    public DeserializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeserializeException(Throwable cause) {
        super(cause);
    }
}
