package io.sundial.console.job;

/**
 * 作业搜索参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 13:53
 */
public class JobSearchParam {
    /**
     * 关键字，可匹配作业组和作业名称
     */
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
