package io.sundial.job.bind;

/**
 * 作业无法解析异常
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 10:29
 */
public class JobUnresolvableException extends JobResolvingException {
    private static final long serialVersionUID = -4673831963992192012L;

    public JobUnresolvableException() {
    }

    public JobUnresolvableException(String message) {
        super(message);
    }

    public JobUnresolvableException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobUnresolvableException(Throwable cause) {
        super(cause);
    }
}
