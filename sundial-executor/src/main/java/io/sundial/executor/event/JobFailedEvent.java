package io.sundial.executor.event;

import io.sundial.job.JobCommand;
import io.sundial.job.JobException;

/**
 * 作业命令已出错事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:14
 */
public class JobFailedEvent extends JobEvent {
    private final JobException jobException;

    public JobFailedEvent(JobCommand jobCommand, JobException jobException) {
        super(jobCommand);
        this.jobException = jobException;
    }

    public JobException getJobException() {
        return jobException;
    }
}
