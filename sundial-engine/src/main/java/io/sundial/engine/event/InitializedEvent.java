package io.sundial.engine.event;

import io.sundial.engine.EngineEvent;

/**
 * 初始化完成事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 16:01
 */
public class InitializedEvent extends EngineEvent {
}
