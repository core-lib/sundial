package io.sundial.util;

/**
 * 字符串工具类
 *
 * @author Payne 646742615@qq.com
 * 2018/12/10 13:07
 */
public class StrKit {

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

}
