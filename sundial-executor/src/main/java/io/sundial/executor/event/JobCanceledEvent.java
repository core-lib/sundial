package io.sundial.executor.event;

import io.sundial.job.JobCommand;

/**
 * 作业取消事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 12:19
 */
public class JobCanceledEvent extends JobEvent {

    public JobCanceledEvent(JobCommand jobCommand) {
        super(jobCommand);
    }
}
