package io.sundial.planning.quartz;

import io.sundial.core.context.Context;
import io.sundial.core.context.Supplier;
import io.sundial.core.context.exception.RoleNotSuppliedException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Quartz调度器提供者
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 10:05
 */
public class SchedulerSupplier implements Supplier<Scheduler> {

    @Override
    public Scheduler supply(Context context) throws RoleNotSuppliedException {
        try {
            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
            return schedulerFactory.getScheduler();
        } catch (SchedulerException e) {
            throw new RoleNotSuppliedException(e);
        }
    }
}
