package io.sundial.assembly;

import java.io.File;
import java.io.IOException;

/**
 * 装包/拆包器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/28 20:18
 */
public interface Assembler {

    /**
     * 格式后缀名
     *
     * @return 后缀名
     */
    String suffix();

    /**
     * 装包
     *
     * @param source 源文件
     * @param target 目标文件
     * @throws IOException I/O 异常
     */
    void assemble(File source, File target) throws IOException;

    /**
     * 拆包
     *
     * @param source 源文件
     * @param target 目标文件
     * @throws IOException I/O 异常
     */
    void disassemble(File source, File target) throws IOException;

}
