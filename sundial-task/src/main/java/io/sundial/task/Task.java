package io.sundial.task;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 任务
 *
 * @author Payne 646742615@qq.com
 * 2018/12/7 17:02
 */
public class Task implements Serializable {
    private static final long serialVersionUID = -715988772894465686L;

    private String name;
    private String group;
    private String cronExpression;
    private String description;
    private int shardingTotal;
    private String shardingTable;
    private String balanceAlgorithm;
    private String misfireInstruction;
    private boolean concurrentAllowed;
    private Date dateStart;
    private Date dateEnd;
    private int priority;
    private String jobName;
    private String jobGroup;
    private Map<String, String> jobArguments;
    private Set<Key> children;

    public static class Key implements Serializable {
        private static final long serialVersionUID = 296624634033585427L;

        private final String name;
        private final String group;

        public Key(String name, String group) {
            this.name = name;
            this.group = group;
        }

        public String getName() {
            return name;
        }

        public String getGroup() {
            return group;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (name != null ? !name.equals(key.name) : key.name != null) return false;
            return group != null ? group.equals(key.group) : key.group == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (group != null ? group.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "" + group + "/" + name;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getShardingTotal() {
        return shardingTotal;
    }

    public void setShardingTotal(int shardingTotal) {
        this.shardingTotal = shardingTotal;
    }

    public String getShardingTable() {
        return shardingTable;
    }

    public void setShardingTable(String shardingTable) {
        this.shardingTable = shardingTable;
    }

    public String getBalanceAlgorithm() {
        return balanceAlgorithm;
    }

    public void setBalanceAlgorithm(String balanceAlgorithm) {
        this.balanceAlgorithm = balanceAlgorithm;
    }

    public String getMisfireInstruction() {
        return misfireInstruction;
    }

    public void setMisfireInstruction(String misfireInstruction) {
        this.misfireInstruction = misfireInstruction;
    }

    public boolean isConcurrentAllowed() {
        return concurrentAllowed;
    }

    public void setConcurrentAllowed(boolean concurrentAllowed) {
        this.concurrentAllowed = concurrentAllowed;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public Map<String, String> getJobArguments() {
        return jobArguments;
    }

    public void setJobArguments(Map<String, String> jobArguments) {
        this.jobArguments = jobArguments;
    }

    public Set<Key> getChildren() {
        return children;
    }

    public void setChildren(Set<Key> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        return group != null ? group.equals(task.group) : task.group == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "" + group + "/" + name;
    }
}
