package io.sundial.planning.quartz;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;

/**
 * Quartz计划
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 12:18
 */
public class Plan {
    private final JobDetail jobDetail;
    private final CronTrigger cronTrigger;

    public Plan(JobDetail jobDetail, CronTrigger cronTrigger) {
        this.jobDetail = jobDetail;
        this.cronTrigger = cronTrigger;
    }

    public JobDetail getJobDetail() {
        return jobDetail;
    }

    public CronTrigger getCronTrigger() {
        return cronTrigger;
    }
}
