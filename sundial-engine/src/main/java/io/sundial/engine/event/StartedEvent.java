package io.sundial.engine.event;

import io.sundial.engine.EngineEvent;

/**
 * 启动完成事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 16:02
 */
public class StartedEvent extends EngineEvent {
}
