package io.sundial.job.bind;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

/**
 * 作业实际参数
 */
public class JobData {
    private final Map<String, String> data;

    public JobData(Map<String, String> data) {
        this.data = data;
    }

    public int size() {
        return data.size();
    }

    public boolean containsKey(Object key) {
        return data.containsKey(key);
    }

    public String get(Object key) {
        return data.get(key);
    }

    public Enumeration<String> keySet() {
        return Collections.enumeration(data.keySet());
    }

    public Enumeration<String> values() {
        return Collections.enumeration(data.values());
    }

    public Enumeration<Map.Entry<String, String>> entrySet() {
        return Collections.enumeration(data.entrySet());
    }
}