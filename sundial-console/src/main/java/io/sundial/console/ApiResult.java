package io.sundial.console;

/**
 * API结果超类
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 13:05
 */
public class ApiResult {

    /**
     * 请求执行成功标记
     * 只有{@code 0}代表请求执行成功，除了{@code 0}其他标记都认为是失败。
     */
    public static final int SUCCESS = 0;
    public static final ApiResult OK = ok("OK");

    private int code = SUCCESS;
    private String message = "OK";

    public ApiResult() {
    }

    public ApiResult(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ApiResult ok() {
        return OK;
    }

    public static ApiResult ok(String message) {
        return valueOf(SUCCESS, message);
    }

    public static ApiResult valueOf(int code, String message) {
        return new ApiResult(code, message);
    }

    public boolean success() {
        return code == SUCCESS;
    }

    public <T extends ApiResult> T success(String message) {
        return error(SUCCESS, message);
    }

    public <T extends ApiResult> T error(int code, String message) {
        this.code = code;
        this.message = message;
        return (T) this;
    }

    public <T extends ApiResult> T wrap(ApiResult result) {
        return error(result.getCode(), result.getMessage());
    }

    /**
     * 请求执行结果标记
     * 判断请求是否成功执行的唯一判断
     *
     * @return 请求执行结果标记
     */
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 请求执行结果消息
     * 请求执行结果的一个友好消息，不推荐通过该返回值判断请求的执行结果。
     *
     * @return 请求执行结果消息
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "" + code + " " + message;
    }

}
