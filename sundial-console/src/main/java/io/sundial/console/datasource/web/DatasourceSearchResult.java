package io.sundial.console.datasource.web;

import io.sundial.console.ApiResult;
import io.sundial.console.datasource.core.Datasource;

import java.util.List;

/**
 * 数据源分页搜索结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 10:50
 */
public class DatasourceSearchResult extends ApiResult {
    /**
     * 分页查询参数
     */
    private DatasourceSearchParam param;
    /**
     * 总匹配条数
     */
    private long total;
    /**
     * 当前分页条目列表
     */
    private List<Datasource> items;

    public DatasourceSearchResult() {
    }

    public DatasourceSearchResult(DatasourceSearchParam param) {
        this.param = param;
    }

    public DatasourceSearchParam getParam() {
        return param;
    }

    public void setParam(DatasourceSearchParam param) {
        this.param = param;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<Datasource> getItems() {
        return items;
    }

    public void setItems(List<Datasource> items) {
        this.items = items;
    }
}
