package io.sundial.core;

/**
 * 框架异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:46
 */
public abstract class SundialException extends Exception {
    private static final long serialVersionUID = -1356545247867471173L;

    public SundialException() {
    }

    public SundialException(String message) {
        super(message);
    }

    public SundialException(String message, Throwable cause) {
        super(message, cause);
    }

    public SundialException(Throwable cause) {
        super(cause);
    }
}
