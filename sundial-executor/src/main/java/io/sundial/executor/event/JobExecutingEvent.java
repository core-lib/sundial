package io.sundial.executor.event;

import io.sundial.job.JobCommand;

/**
 * 作业命令执行中事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:13
 */
public class JobExecutingEvent extends JobEvent {

    public JobExecutingEvent(JobCommand jobCommand) {
        super(jobCommand);
    }
}
