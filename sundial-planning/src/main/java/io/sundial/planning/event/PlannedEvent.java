package io.sundial.planning.event;

import io.sundial.planning.PlannerEvent;
import io.sundial.task.Task;

/**
 * 计划内事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 13:12
 */
public class PlannedEvent extends PlannerEvent {
    private final Task task;
    private final long time;

    public PlannedEvent(Task task, long time) {
        this.task = task;
        this.time = time;
    }

    public Task getTask() {
        return task;
    }

    public long getTime() {
        return time;
    }
}
