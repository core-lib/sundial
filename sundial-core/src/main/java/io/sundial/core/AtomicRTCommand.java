package io.sundial.core;


/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:02
 */
public interface AtomicRTCommand<R, E extends Exception> extends AtomicCommand {

    R run() throws E;

}
