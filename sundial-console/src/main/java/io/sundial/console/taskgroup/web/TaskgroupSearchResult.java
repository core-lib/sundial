package io.sundial.console.taskgroup.web;

import io.sundial.console.ApiResult;
import io.sundial.console.taskgroup.core.Taskgroup;

import java.util.List;

/**
 * 数据源分页搜索结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 10:50
 */
public class TaskgroupSearchResult extends ApiResult {
    /**
     * 分页查询参数
     */
    private TaskgroupSearchParam param;
    /**
     * 总匹配条数
     */
    private long total;
    /**
     * 当前分页条目列表
     */
    private List<Taskgroup> items;

    public TaskgroupSearchResult() {
    }

    public TaskgroupSearchResult(TaskgroupSearchParam param) {
        this.param = param;
    }

    public TaskgroupSearchParam getParam() {
        return param;
    }

    public void setParam(TaskgroupSearchParam param) {
        this.param = param;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<Taskgroup> getItems() {
        return items;
    }

    public void setItems(List<Taskgroup> items) {
        this.items = items;
    }
}
