package io.sundial.balance;

/**
 * 作业均衡
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 13:14
 */
public interface Balancing {

    /**
     * 挂载一个作业均衡器，如果作业均衡器已存在或挂载失败则返回{@code false}，否则返回{@code true}
     *
     * @param balancer 作业均衡器
     * @return 挂载成功：true 否则：false
     */
    boolean mount(Balancer balancer);

    /**
     * 卸载一个作业均衡器，如果作业均衡器不存在或卸载失败则返回{@code false}，否则返回{@code true}
     *
     * @param balancer 作业均衡器
     * @return 卸载成功：true 否则：false
     */
    boolean unmount(Balancer balancer);

}
