package io.sundial.job;

import java.io.Serializable;
import java.util.Map;

/**
 * 作业命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 12:10
 */
public class JobCommand implements Comparable<JobCommand>, Serializable {
    private static final long serialVersionUID = 7577791926286340105L;

    private String jobPath;
    private long jobTime;
    private JobSharding jobSharding;
    private Map<String, String> jobArguments;

    @Override
    public int compareTo(JobCommand that) {
        return Long.compare(this.jobTime, that.jobTime);
    }

    public String getJobPath() {
        return jobPath;
    }

    public void setJobPath(String jobPath) {
        this.jobPath = jobPath;
    }

    public long getJobTime() {
        return jobTime;
    }

    public void setJobTime(long jobTime) {
        this.jobTime = jobTime;
    }

    public JobSharding getJobSharding() {
        return jobSharding;
    }

    public void setJobSharding(JobSharding jobSharding) {
        this.jobSharding = jobSharding;
    }

    public Map<String, String> getJobArguments() {
        return jobArguments;
    }

    public void setJobArguments(Map<String, String> jobArguments) {
        this.jobArguments = jobArguments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobCommand that = (JobCommand) o;

        if (jobTime != that.jobTime) return false;
        return jobSharding != null ? jobSharding.equals(that.jobSharding) : that.jobSharding == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (jobTime ^ (jobTime >>> 32));
        result = 31 * result + (jobSharding != null ? jobSharding.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "/" + jobSharding + "/" + jobTime;
    }
}
