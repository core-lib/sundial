package io.sundial.console.job;

import com.google.common.base.Preconditions;
import io.sundial.job.JobDefinition;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 作业缓存
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 11:28
 */
public class JobCache {
    private final AtomicReference<JobDefinition> jobDefinition;
    private final ConcurrentMap<String, JobDefinition> jobExecutors;

    public JobCache(JobDefinition jobDefinition) {
        Preconditions.checkNotNull(jobDefinition, "job definition must not be null");
        this.jobDefinition = new AtomicReference<>(jobDefinition);
        this.jobExecutors = new ConcurrentHashMap<>();
    }

    public AtomicReference<JobDefinition> getJobDefinition() {
        return jobDefinition;
    }

    public ConcurrentMap<String, JobDefinition> getJobExecutors() {
        return jobExecutors;
    }

    @Override
    public String toString() {
        return jobDefinition.toString();
    }
}
