package io.sundial.hibernate;

import io.sundial.core.context.Context;
import io.sundial.core.context.Supplier;
import io.sundial.core.context.exception.RoleNotSuppliedException;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * SessionFactory提供者
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 14:52
 */
public class SessionFactorySupplier implements Supplier<SessionFactory> {

    @Override
    public SessionFactory supply(Context context) throws RoleNotSuppliedException {
        try {
            Configuration configuration = new Configuration().configure();
            return configuration.buildSessionFactory();
        } catch (HibernateException e) {
            throw new RoleNotSuppliedException(e);
        }
    }
}
