package io.sundial.core.event;

/**
 * 事件监听器
 * 子类可通过指定类型参数来声明自己感兴趣的事件类型，同时指定的事件类型的子类也会通知该监听器。
 *
 * @author Payne 646742615@qq.com
 * 2018/12/17 10:36
 */
public interface EventListener<E extends Event> {

    /**
     * 当所关心的事件发生时回调，如果监听的是一个事件的父类，则监听方法需要通过事件的类型来判断当前发生的何种事件。
     *
     * @param event 事件
     */
    void onListened(E event) throws Exception;

}
