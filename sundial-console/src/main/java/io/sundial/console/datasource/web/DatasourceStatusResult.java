package io.sundial.console.datasource.web;

import io.sundial.console.ApiResult;

/**
 * 数据源状态切换结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 12:55
 */
public class DatasourceStatusResult extends ApiResult {
}
