package io.sundial.coordination;

import io.sundial.core.SundialException;

/**
 * 协调者异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 13:30
 */
public class CoordinatorException extends SundialException {
    private static final long serialVersionUID = 352652834695851131L;

    public CoordinatorException() {
    }

    public CoordinatorException(String message) {
        super(message);
    }

    public CoordinatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CoordinatorException(Throwable cause) {
        super(cause);
    }
}
