package io.sundial.console.datasource.web;

import io.sundial.console.ApiResult;

/**
 * 数据源验证结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 15:30
 */
public class DatasourceVerifyResult extends ApiResult {
}
