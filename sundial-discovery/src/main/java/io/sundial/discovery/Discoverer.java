package io.sundial.discovery;

import io.sundial.core.event.EventListenable;
import io.sundial.core.lifecycle.Lifecycle;
import io.sundial.discovery.exception.DiscoveringException;
import io.sundial.job.Job;

import java.util.Enumeration;

/**
 * 作业发现者接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 16:54
 */
public interface Discoverer extends Lifecycle, EventListenable<DiscovererEvent> {

    /**
     * 根据作业名称获取作业实例，当作业不存在时抛出{@link DiscoveringException}异常。
     *
     * @param jobName 作业名称
     * @param jobGroup 作业分组
     * @return 作业实例
     * @throws DiscoveringException 作业不存在异常
     */
    Job discover(String jobName, String jobGroup) throws DiscoveringException;

    /**
     * 获取作业枚举器
     *
     * @return 作业枚举器
     */
    Enumeration<Job> jobs();

}
