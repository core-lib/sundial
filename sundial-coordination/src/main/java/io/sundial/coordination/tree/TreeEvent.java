package io.sundial.coordination.tree;

import com.google.common.base.Preconditions;
import io.sundial.coordination.node.Node;

/**
 * 节点树事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 11:08
 */
public class TreeEvent {
    private final String root;
    private final Type type;
    private final Tree tree;
    private final Node node;

    public TreeEvent(String root, Type type, Tree tree, Node node) {
        Preconditions.checkNotNull(root, "root must not be null");
        Preconditions.checkNotNull(type, "type must not be null");
        Preconditions.checkNotNull(tree, "tree must not be null");
        this.root = root;
        this.type = type;
        this.tree = tree;
        this.node = node;
    }

    /**
     * 节点树事件
     */
    public enum Type {
        /**
         * 子节点创建
         */
        NODE_CREATED,
        /**
         * 子节点更新
         */
        NODE_UPDATED,
        /**
         * 子节点删除
         */
        NODE_REMOVED,

        /**
         * 连接断开
         */
        CONN_SUSPENDED,
        /**
         * 连接重连
         */
        CONN_RECONNECTED,
        /**
         * 连接丢失
         */
        CONN_LOST,
        /**
         * 节点树已初始化
         */
        TREE_INITIALIZED
    }

    public String getRoot() {
        return root;
    }

    public Type getType() {
        return type;
    }

    public Tree getTree() {
        return tree;
    }

    public Node getNode() {
        return node;
    }

}
