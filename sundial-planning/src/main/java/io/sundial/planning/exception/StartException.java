package io.sundial.planning.exception;

import io.sundial.planning.PlannerException;

/**
 * 启动异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 17:55
 */
public class StartException extends PlannerException {
    private static final long serialVersionUID = 5124115069636852102L;

    public StartException() {
    }

    public StartException(String message) {
        super(message);
    }

    public StartException(String message, Throwable cause) {
        super(message, cause);
    }

    public StartException(Throwable cause) {
        super(cause);
    }
}
