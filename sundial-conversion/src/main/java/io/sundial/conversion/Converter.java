package io.sundial.conversion;

import io.sundial.conversion.exception.DeserializeException;
import io.sundial.conversion.exception.SerializeException;

/**
 * 转换器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:27
 */
public interface Converter {

    /**
     * 获取转换器的支持内容类型名称
     *
     * @return 支持内容类型
     */
    String getContentType();

    /**
     * 将对象序列化成byte数组
     *
     * @param obj 对象
     * @return byte 数组
     * @throws SerializeException 序列化异常
     */
    byte[] serialize(Object obj) throws SerializeException;

    /**
     * 将byte数组序列化成对象
     *
     * @param data byte 数组
     * @return 对象
     * @throws DeserializeException 反序列化异常
     */
    <T> T deserialize(byte[] data, Class<T> type) throws DeserializeException;

}
