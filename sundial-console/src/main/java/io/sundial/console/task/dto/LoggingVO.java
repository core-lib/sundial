package io.sundial.console.task.dto;

import io.sundial.console.task.core.Logging;

import java.util.Date;

/**
 * 执行记录VO
 *
 * @author Payne 646742615@qq.com
 * 2019/1/14 15:54
 */
public class LoggingVO {
    private Date time;
    private Logging.Status status;
    private Integer code;
    private String message;
    private String result;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Logging.Status getStatus() {
        return status;
    }

    public void setStatus(Logging.Status status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
