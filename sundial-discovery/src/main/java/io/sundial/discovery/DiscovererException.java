package io.sundial.discovery;

import io.sundial.core.SundialException;

/**
 * 发现者异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:08
 */
public abstract class DiscovererException extends SundialException {
    private static final long serialVersionUID = 7079794530244535758L;

    public DiscovererException() {
    }

    public DiscovererException(String message) {
        super(message);
    }

    public DiscovererException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiscovererException(Throwable cause) {
        super(cause);
    }
}
