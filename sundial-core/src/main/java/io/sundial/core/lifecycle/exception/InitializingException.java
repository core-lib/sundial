package io.sundial.core.lifecycle.exception;

import io.sundial.core.lifecycle.LifecycleException;

/**
 * 初始化异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:51
 */
public class InitializingException extends LifecycleException {
    private static final long serialVersionUID = -6147503917732991795L;

    public InitializingException() {
    }

    public InitializingException(String message) {
        super(message);
    }

    public InitializingException(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializingException(Throwable cause) {
        super(cause);
    }
}
