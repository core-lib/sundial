package io.sundial.core;


/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:08
 */
public interface AtomicRNCommand<R> extends AtomicCommand {

    R run();

}
