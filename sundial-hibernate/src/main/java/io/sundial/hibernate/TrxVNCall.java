package io.sundial.hibernate;


import io.sundial.core.AtomicCommand;
import org.hibernate.Session;

/**
 * 命令
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:07
 */
public interface TrxVNCall extends AtomicCommand {

    void call(Session sex);

}
