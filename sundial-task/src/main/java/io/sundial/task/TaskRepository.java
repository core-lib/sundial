package io.sundial.task;

import io.sundial.repository.Repository;

/**
 * 任务仓储
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 10:51
 */
public interface TaskRepository extends Repository<Task.Key, Task> {
}
