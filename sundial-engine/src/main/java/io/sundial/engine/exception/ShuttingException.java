package io.sundial.engine.exception;

import io.sundial.engine.EngineException;

/**
 * 引擎关闭异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 14:13
 */
public class ShuttingException extends EngineException {
    private static final long serialVersionUID = 1015965628694769970L;

    public ShuttingException() {
    }

    public ShuttingException(String message) {
        super(message);
    }

    public ShuttingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShuttingException(Throwable cause) {
        super(cause);
    }
}
