package io.sundial.coordination.vote;

import io.sundial.coordination.ConnStatus;
import io.sundial.coordination.Coordinator;

/**
 * 选举监听器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 11:23
 */
public interface VoteWatcher {

    /**
     * 当自己当选时回调
     * 需要注意的是，如果方法抛出异常或正常返回后领导权将自动释放，可以采用死递归的方式维持自己的领导权。
     *
     * @param coordinator 协调者
     */
    void onMyselfElected(Coordinator coordinator) throws Exception;

    /**
     * 当连接状态变化时回调
     * 如果连接状态的{@link ConnStatus#connected} 属性为{@code false} 时最好返回{@code false}来声明放弃领导权，
     * 如果返回为{@code false}，{@link VoteWatcher#onMyselfElected(Coordinator)}方法的执行将被中断。
     *
     * @param coordinator 协调者
     * @param connStatus  连接状态
     * @return 是否维持领导权
     */
    boolean onStatusChanged(Coordinator coordinator, ConnStatus connStatus) throws Exception;

}
