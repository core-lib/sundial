package io.sundial.executor.event;

import io.sundial.job.JobCommand;

/**
 * 作业拒绝事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 12:17
 */
public class JobRejectedEvent extends JobEvent {

    public JobRejectedEvent(JobCommand jobCommand) {
        super(jobCommand);
    }
}
