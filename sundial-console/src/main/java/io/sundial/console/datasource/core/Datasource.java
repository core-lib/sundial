package io.sundial.console.datasource.core;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * 数据源
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 16:02
 */
@Entity
@Table(name = "sundial_datasource")
@EntityListeners(AuditingEntityListener.class)
public class Datasource {

    @Id
    @Column(name = "name", length = 36, nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", length = 36, nullable = false)
    private DatasourceType type;

    @Column(name = "address", length = 64, nullable = false)
    private String address;

    @Column(name = "catalog", length = 36, nullable = false)
    private String catalog;

    @Column(name = "username", length = 36, nullable = false)
    private String username;

    @Column(name = "password", length = 36, nullable = false)
    private String password;

    @Column(name = "description", length = 240, nullable = false)
    private String description;

    @Version
    @Column(name = "version", length = 20, nullable = false)
    private Long version;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "date_created", nullable = false)
    private Date dateCreated;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "last_updated", nullable = false)
    private Date lastUpdated;

    @Column(name = "disabled", length = 1, nullable = false)
    private Boolean disabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DatasourceType getType() {
        return type;
    }

    public void setType(DatasourceType type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Datasource that = (Datasource) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
