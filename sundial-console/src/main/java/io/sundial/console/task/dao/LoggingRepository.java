package io.sundial.console.task.dao;

import io.sundial.console.task.core.Logging;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
 * 执行记录仓储接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/14 13:33
 */
public interface LoggingRepository extends JpaRepository<Logging, Logging.Key> {

    @Query("FROM Logging AS l WHERE l.key.taskGroup = :taskGroup AND l.key.taskName = :taskName AND l.dateCreated >= :dateStart AND l.dateCreated <= :dateEnd")
    Page<Logging> search(
            @Param("taskGroup") String taskGroup,
            @Param("taskName") String taskName,
            @Param("dateStart") Date dateStart,
            @Param("dateEnd") Date dateEnd,
            Pageable pageable
    );

    @Query("FROM Logging AS l WHERE l.key.taskGroup = :taskGroup AND l.key.taskName = :taskName AND l.dateCreated >= :dateStart AND l.dateCreated <= :dateEnd AND l.status = :status")
    Page<Logging> search(
            @Param("taskGroup") String taskGroup,
            @Param("taskName") String taskName,
            @Param("dateStart") Date dateStart,
            @Param("dateEnd") Date dateEnd,
            @Param("status") Logging.Status status,
            Pageable pageable
    );

}
