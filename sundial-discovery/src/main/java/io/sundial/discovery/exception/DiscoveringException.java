package io.sundial.discovery.exception;

import io.sundial.discovery.DiscovererException;

/**
 * 作业发现异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 16:56
 */
public class DiscoveringException extends DiscovererException {
    private static final long serialVersionUID = -5609299198483662801L;

    public DiscoveringException() {
    }

    public DiscoveringException(String message) {
        super(message);
    }

    public DiscoveringException(String message, Throwable cause) {
        super(message, cause);
    }

    public DiscoveringException(Throwable cause) {
        super(cause);
    }
}
