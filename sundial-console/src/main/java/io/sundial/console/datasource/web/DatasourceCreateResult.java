package io.sundial.console.datasource.web;

import io.sundial.console.ApiResult;

/**
 * 数据源创建结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 15:23
 */
public class DatasourceCreateResult extends ApiResult {

}
