package io.sundial.console.datasource.service;

import io.sundial.console.Pagination;
import io.sundial.console.datasource.core.Datasource;

/**
 * 数据源服务接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 16:18
 */
public interface DatasourceService {

    Datasource find(String name);

    void save(Datasource datasource);

    Pagination<Datasource> search(int pageNo, int pageSize, String keyword);

    void shift(String name, boolean disabled);

    void remove(String name);

    void update(String name, Datasource datasource);

}
