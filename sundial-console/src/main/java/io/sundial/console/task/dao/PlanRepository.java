package io.sundial.console.task.dao;

import io.sundial.console.task.core.Plan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 计划仓储接口
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 12:12
 */
public interface PlanRepository extends JpaRepository<Plan, Plan.Key> {

    @Query("FROM Plan AS p WHERE p.key.name LIKE :keyword OR p.key.group LIKE :keyword OR p.description LIKE :keyword")
    Page<Plan> search(@Param("keyword") String keyword, Pageable pageable);

    @Query("FROM Plan AS p WHERE p.key.group = :group AND (p.key.name LIKE :keyword OR p.key.group LIKE :keyword OR p.description LIKE :keyword)")
    Page<Plan> search(@Param("keyword") String keyword, @Param("group") String group, Pageable pageable);

}
