package io.sundial.job.bind.impl;

import io.sundial.job.JobContext;
import io.sundial.job.JobParameter;
import io.sundial.job.bind.JobData;
import io.sundial.job.bind.JobField;
import io.sundial.job.bind.JobResolver;
import io.sundial.util.StrKit;

/**
 * String 转换器
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 17:39
 */
public class StringResolver implements JobResolver {

    @Override
    public boolean supports(JobField field) {
        return field.type == String.class
                || field.type == StringBuilder.class
                || field.type == StringBuffer.class
                || field.type == CharSequence.class;
    }

    @Override
    public JobParameter resolve(JobField field) {
        String value = field.value != null ? field.value.toString() : null;
        return new JobParameter(field.name)
                .setEditor("textarea")
                .setValue(value);
    }

    @Override
    public Object resolve(JobField field, JobData data, JobContext context) {
        String value = data.get(field.name);
        return StrKit.isEmpty(value) ? DEFAULT
                : field.type == String.class ? value
                : field.type == StringBuilder.class ? new StringBuilder(value)
                : field.type == StringBuffer.class ? new StringBuffer(value)
                : field.type == CharSequence.class ? value
                : DEFAULT;
    }
}
