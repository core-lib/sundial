package io.sundial.console.authentication;

import io.sundial.console.ApiResult;

/**
 * 认证结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 13:09
 */
public class AuthenticateResult extends ApiResult {
    private long expiresIn;
    private String token;

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthenticateResult that = (AuthenticateResult) o;

        return token != null ? token.equals(that.token) : that.token == null;
    }

    @Override
    public int hashCode() {
        return token != null ? token.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "" + token + "(" + expiresIn + "s)";
    }
}
