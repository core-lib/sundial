package io.sundial.core.lifecycle;

import io.sundial.core.context.Context;
import io.sundial.core.lifecycle.exception.DestroyingException;
import io.sundial.core.lifecycle.exception.InitializingException;

/**
 * 生命周期接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 9:43
 */
public interface Lifecycle {

    /**
     * 获取状态
     *
     * @return 状态
     */
    State state();

    /**
     * 初始化
     * 实现类对该方法的实现需要保证该方法是幂等的，因为框架有可能会重复调用该方法。
     * 可以尽量通过继承{@link Stateful}并且重写{@link Stateful#initializing(Context)}方法来进行初始化操作，
     * {@link Stateful#initialize(Context)}可以保证幂等性。
     *
     * @param context 框架上下文
     * @throws InitializingException 初始化异常
     */
    void initialize(Context context) throws InitializingException;

    /**
     * 销毁
     * 实现类对该方法的实现需要保证该方法是幂等的，因为框架有可能会重复调用该方法。
     * 可以尽量通过继承{@link Stateful}并且重写{@link Stateful#destroying()}方法来进行销毁操作，
     * {@link Stateful#destroy()}可以保证幂等性。
     *
     * @throws DestroyingException 销毁异常
     */
    void destroy() throws DestroyingException;

    /**
     * 对象生命周期状态
     */
    enum State {
        /**
         * 未初始化
         */
        NONE("none"),

        /**
         * 初始化中
         */
        INITIALIZING("initializing"),
        /**
         * 已初始化
         */
        INITIALIZED("initialized"),

        /**
         * 销毁中
         */
        DESTROYING("destroying"),
        /**
         * 已销毁
         */
        DESTROYED("destroyed");

        public final String value;

        State(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

}
