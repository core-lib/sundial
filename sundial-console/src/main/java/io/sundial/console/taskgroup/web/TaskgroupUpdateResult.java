package io.sundial.console.taskgroup.web;

import io.sundial.console.ApiResult;

/**
 * 数据源更新结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/4 13:26
 */
public class TaskgroupUpdateResult extends ApiResult {
}
