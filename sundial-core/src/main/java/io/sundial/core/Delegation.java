package io.sundial.core;

/**
 * 委派抽象类
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 21:43
 */
public abstract class Delegation<T> implements Delegate<T> {
    private final Object mutex = new Object();
    protected volatile T target;

    @Override
    public T get() {
        if (target != null) {
            return target;
        }
        synchronized (mutex) {
            if (target != null) {
                return target;
            }
            target = build();
        }
        return target;
    }

    protected abstract T build();

}
