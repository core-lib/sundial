package io.sundial.assembly;

/**
 * WAR格式
 *
 * @author Payne 646742615@qq.com
 * 2018/12/28 22:07
 */
public class WarAssembler extends ZipAssembler {

    @Override
    public String suffix() {
        return "war";
    }
}
