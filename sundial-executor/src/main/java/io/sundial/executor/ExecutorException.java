package io.sundial.executor;

import io.sundial.engine.EngineException;

/**
 * 执行器异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:07
 */
public abstract class ExecutorException extends EngineException {
    private static final long serialVersionUID = -4211515682609372363L;

    public ExecutorException() {
    }

    public ExecutorException(String message) {
        super(message);
    }

    public ExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExecutorException(Throwable cause) {
        super(cause);
    }
}
