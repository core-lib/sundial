package io.sundial.job.bind;

import io.sundial.job.JobContext;
import io.sundial.job.JobParameter;

/**
 * 作业参数解析器
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 17:35
 */
public interface JobResolver {

    /**
     * 缺省值
     */
    Object DEFAULT = new Object();

    /**
     * 是否支持对该作业参数的解析
     *
     * @param field 作业形式参数
     * @return true: 支持  false: 不支持
     */
    boolean supports(JobField field);

    /**
     * 将作业形式参数解析成一个或多个作业参数定义
     *
     * @param field 作业形式参数
     * @return 解析后的作业参数定义
     * @throws JobResolvingException 作业解析异常
     * @see JobUnresolvableException 作业无法解析异常
     */
    JobParameter resolve(JobField field) throws JobResolvingException;

    /**
     * 将作业实际参数解析成参数值
     *
     * @param field   作业形式参数
     * @param data    作业实际参数
     * @param context 作业上下文
     * @return 作业参数实际值
     * @throws Exception 解析异常
     * @see JobResolvingException 作业解析异常
     * @see JobUnresolvableException 作业无法解析异常
     */
    Object resolve(JobField field, JobData data, JobContext context) throws Exception;
}
