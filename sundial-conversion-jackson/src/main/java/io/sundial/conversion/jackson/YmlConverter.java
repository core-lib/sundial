package io.sundial.conversion.jackson;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import io.sundial.conversion.Converter;
import io.sundial.conversion.exception.DeserializeException;
import io.sundial.conversion.exception.SerializeException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * YML转换器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/25 15:40
 */
public class YmlConverter implements Converter {
    private YAMLMapper ymlMapper = new YAMLMapper();

    @Override
    public String getContentType() {
        return "application/yml";
    }

    @Override
    public byte[] serialize(Object obj) throws SerializeException {
        try (
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ) {
            ymlMapper.writeValue(bos, obj);
            return bos.toByteArray();
        } catch (IOException e) {
            throw new SerializeException(e);
        }
    }

    @Override
    public <T> T deserialize(byte[] data, Class<T> type) throws DeserializeException {
        try (
                ByteArrayInputStream bis = new ByteArrayInputStream(data)
        ) {
            return ymlMapper.readValue(bis, type);
        } catch (IOException e) {
            throw new DeserializeException(e);
        }
    }

    public YAMLMapper getYmlMapper() {
        return ymlMapper;
    }

    public void setYmlMapper(YAMLMapper ymlMapper) {
        this.ymlMapper = ymlMapper;
    }
}
