package io.sundial.application.impl;

import io.sundial.application.Application;
import io.sundial.core.context.Context;

/**
 * 应用关闭器
 *
 * @author Payne 646742615@qq.com
 * 2019/1/23 11:03
 */
public class AppShutter {

    public static void main(String... args) throws Exception {
        Context context = new StandaloneContext();
        Application application = context.get(Application.class);

    }

}
