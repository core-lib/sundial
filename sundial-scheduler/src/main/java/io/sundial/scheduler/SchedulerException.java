package io.sundial.scheduler;

import io.sundial.executor.ExecutorException;

/**
 * 调度器异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 10:35
 */
public class SchedulerException extends ExecutorException {
    private static final long serialVersionUID = -2731798681022876181L;

    public SchedulerException() {
    }

    public SchedulerException(String message) {
        super(message);
    }

    public SchedulerException(String message, Throwable cause) {
        super(message, cause);
    }

    public SchedulerException(Throwable cause) {
        super(cause);
    }
}
