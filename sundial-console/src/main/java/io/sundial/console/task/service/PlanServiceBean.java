package io.sundial.console.task.service;

import io.sundial.console.Pagination;
import io.sundial.console.task.core.Plan;
import io.sundial.console.task.dao.PlanRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * 计划Service实现
 *
 * @author Payne 646742615@qq.com
 * 2019/1/9 12:48
 */
@Service
public class PlanServiceBean implements PlanService {

    @Resource
    private PlanRepository planRepository;

    @Override
    public Pagination<Plan> search(String keyword, String group, int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
        Page<Plan> page = StringUtils.isEmpty(group)
                ? planRepository.search("%" + (keyword != null ? keyword : "") + "%", pageable)
                : planRepository.search("%" + (keyword != null ? keyword : "") + "%", group, pageable);
        return new Pagination<>(page.getTotalElements(), page.getContent());
    }
}
