package io.sundial.planning.event;

import io.sundial.planning.PlannerEvent;
import io.sundial.task.Task;

/**
 * 任务删除事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/21 14:38
 */
public class RemovedEvent extends PlannerEvent {
    private final Task task;

    public RemovedEvent(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }
}
