package io.sundial.coordination.node;

/**
 * 节点
 *
 * @author Payne 646742615@qq.com
 * 2018/12/23 16:48
 */
public interface Node {

    /**
     * @return 节点路径
     */
    String getPath();

    /**
     * @return 节点数据
     */
    byte[] getData();

    /**
     * 获取节点数据版本
     *
     * @return 节点数据版本
     */
    int getVersion();

}
