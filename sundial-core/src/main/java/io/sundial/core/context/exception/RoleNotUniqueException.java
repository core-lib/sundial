package io.sundial.core.context.exception;

import io.sundial.core.context.RoleException;

/**
 * 角色不唯一异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:30
 */
public class RoleNotUniqueException extends RoleException {
    private static final long serialVersionUID = 214648674252640202L;

    public RoleNotUniqueException() {
    }

    public RoleNotUniqueException(String message) {
        super(message);
    }

    public RoleNotUniqueException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleNotUniqueException(Throwable cause) {
        super(cause);
    }
}
