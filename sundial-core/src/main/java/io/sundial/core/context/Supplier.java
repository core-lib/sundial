package io.sundial.core.context;

import io.sundial.core.context.exception.RoleNotSuppliedException;

/**
 * 提供器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 22:50
 */
public interface Supplier<T> {

    /**
     * 提供
     *
     * @return 提供对象
     */
    T supply(Context context) throws RoleNotSuppliedException;

}
