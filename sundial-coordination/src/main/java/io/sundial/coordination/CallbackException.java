package io.sundial.coordination;

/**
 * 回调错误
 *
 * @author Payne 646742615@qq.com
 * 2019/1/5 11:38
 */
public class CallbackException extends CoordinatorException {
    private static final long serialVersionUID = 7671998949605857347L;

    private final int code;
    private final Object context;

    public CallbackException(int code, Object context) {
        this.code = code;
        this.context = context;
    }

    public CallbackException(String message, int code, Object context) {
        super(message);
        this.code = code;
        this.context = context;
    }

    public CallbackException(String message, Throwable cause, int code, Object context) {
        super(message, cause);
        this.code = code;
        this.context = context;
    }

    public CallbackException(Throwable cause, int code, Object context) {
        super(cause);
        this.code = code;
        this.context = context;
    }

    public int getCode() {
        return code;
    }

    public Object getContext() {
        return context;
    }
}
