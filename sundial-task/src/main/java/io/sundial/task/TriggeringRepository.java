package io.sundial.task;

import io.sundial.repository.Repository;

/**
 * 触发记录仓储接口
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 17:08
 */
public interface TriggeringRepository extends Repository<Triggering.Key, Triggering> {

    void toSubmitted(Triggering.Key key);

    void toRejected(Triggering.Key key);

    void toCanceled(Triggering.Key key);

    void toExecuting(Triggering.Key key);

    void toCompleted(Triggering.Key key, Integer code, String message, String result);

    void toFailed(Triggering.Key key, String result);

}
