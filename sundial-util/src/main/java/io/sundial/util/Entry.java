package io.sundial.util;

import java.io.Serializable;
import java.util.Map;

/**
 * 键值对
 *
 * @author Payne 646742615@qq.com
 * 2018/12/27 15:20
 */
public class Entry<K, V> implements Map.Entry<K, V>, Serializable {
    private static final long serialVersionUID = -8995362230716277897L;

    private final K key;
    private V value;

    public Entry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public V setValue(V value) {
        V old = this.value;
        this.value = value;
        return old;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entry<?, ?> entry = (Entry<?, ?>) o;

        return key != null ? key.equals(entry.key) : entry.key == null;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "" + key + ": " + value;
    }
}
