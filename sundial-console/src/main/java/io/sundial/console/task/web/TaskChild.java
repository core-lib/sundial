package io.sundial.console.task.web;

/**
 * 子任务
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 16:54
 */
public class TaskChild {
    private String group;
    private String name;

    public TaskChild() {
    }

    public TaskChild(String group, String name) {
        this.group = group;
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
