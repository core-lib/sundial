package io.sundial.core.context;

import io.sundial.util.TypKit;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 类型变量
 *
 * @author Payne 646742615@qq.com
 * 2018/12/22 23:39
 */
public abstract class Clazz<T> {
    private final Class<T> rawClass;

    @SuppressWarnings("unchecked")
    protected Clazz() {
        Type derived = TypKit.derive(this.getClass(), Clazz.class, 0);
        if (derived instanceof Class<?>) {
            rawClass = (Class<T>) derived;
        } else if (derived instanceof ParameterizedType) {
            rawClass = (Class<T>) ((ParameterizedType) derived).getRawType();
        } else {
            throw new IllegalArgumentException("could not derive raw class from " + this.getClass());
        }
    }

    public Class<T> getRawClass() {
        return rawClass;
    }
}
