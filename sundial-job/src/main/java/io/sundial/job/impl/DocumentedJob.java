package io.sundial.job.impl;

import io.sundial.job.JobParameter;

import java.util.Map;

/**
 * 注释化作业
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 23:09
 */
public abstract class DocumentedJob extends ParameterizedJob {

    @Override
    public Map<String, JobParameter> parameters() {
        return super.parameters();
    }

    @Override
    public String name() {
        return super.name();
    }

    @Override
    public String group() {
        return super.group();
    }

    @Override
    public String type() {
        return super.type();
    }

    @Override
    public String version() {
        return super.version();
    }

    @Override
    public int shardable() {
        return super.shardable();
    }

    @Override
    public String description() {
        return super.description();
    }
}
