package io.sundial.console.job;

import io.sundial.console.ApiResult;
import io.sundial.job.JobKey;

import java.util.List;

/**
 * 作业搜索结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/7 13:55
 */
public class JobSearchResult extends ApiResult {
    /**
     * 作业键，包含作业组和作业名称
     */
    private List<JobKey> jobKeys;

    public List<JobKey> getJobKeys() {
        return jobKeys;
    }

    public void setJobKeys(List<JobKey> jobKeys) {
        this.jobKeys = jobKeys;
    }
}
