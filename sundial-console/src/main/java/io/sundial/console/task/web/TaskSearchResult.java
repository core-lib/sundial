package io.sundial.console.task.web;

import io.sundial.console.ApiResult;
import io.sundial.console.task.dto.TaskVO;

import java.util.List;

/**
 * 任务搜索结果
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 16:09
 */
public class TaskSearchResult extends ApiResult {
    /**
     * 分页查询参数
     */
    private TaskSearchParam param;
    /**
     * 总匹配条数
     */
    private long total;
    /**
     * 当前分页条目列表
     */
    private List<TaskVO> items;

    public TaskSearchParam getParam() {
        return param;
    }

    public void setParam(TaskSearchParam param) {
        this.param = param;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<TaskVO> getItems() {
        return items;
    }

    public void setItems(List<TaskVO> items) {
        this.items = items;
    }
}
