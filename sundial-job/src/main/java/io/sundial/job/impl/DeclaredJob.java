package io.sundial.job.impl;

import io.sundial.job.Job;
import io.sundial.job.JobParameter;

import java.util.Collections;
import java.util.Map;

/**
 * 声明化作业
 * 作业名称为实现类的简单名称
 * 作业分组为实现类的完整包名
 * 作业类型为{@code Java}
 * 作业版本为获取的当前系统毫秒数
 * 作业描述为实现类的完整类名
 *
 * @author Payne 646742615@qq.com
 * 2019/1/6 17:04
 */
public abstract class DeclaredJob implements Job {

    @Override
    public String name() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String group() {
        Package pkg = this.getClass().getPackage();
        return pkg != null ? pkg.getName() : "DEFAULT";
    }

    @Override
    public String type() {
        return "Java";
    }

    @Override
    public String version() {
        return "1.0.0";
    }

    @Override
    public int shardable() {
        return 1;
    }

    @Override
    public Map<String, JobParameter> parameters() {
        return Collections.emptyMap();
    }

    @Override
    public String description() {
        return this.getClass().getName();
    }
}
