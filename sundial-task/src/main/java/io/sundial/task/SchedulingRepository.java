package io.sundial.task;

import io.sundial.repository.Repository;

/**
 * 调度记录仓储
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 10:54
 */
public interface SchedulingRepository extends Repository<Scheduling.Key, Scheduling> {

    void toFinished(Scheduling.Key key);

}
