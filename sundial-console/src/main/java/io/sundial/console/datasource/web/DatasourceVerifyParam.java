package io.sundial.console.datasource.web;

import io.sundial.console.datasource.core.DatasourceType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 数据源验证参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/3 15:30
 */
public class DatasourceVerifyParam {

    /**
     * 数据源名称
     */
    @NotEmpty(message = "数据源名称不能为空")
    @Size(max = 36, message = "数据源名称不能超过36个字符")
    private String name;

    /**
     * 数据源类型
     */
    @NotNull(message = "数据源类型不能为空")
    private DatasourceType type;

    /**
     * 数据源地址
     */
    @NotEmpty(message = "数据源地址不能为空")
    @Size(max = 64, message = "数据源地址不能超过64个字符")
    private String address;

    /**
     * 数据库名称
     */
    @NotEmpty(message = "数据库名称不能为空")
    @Size(max = 36, message = "数据库名称不能超过36个字符")
    private String catalog;

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    @Size(max = 36, message = "用户名不能超过36个字符")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message = "密码不能为空")
    @Size(max = 36, message = "密码不能超过36个字符")
    private String password;

    /**
     * 数据源描述
     */
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DatasourceType getType() {
        return type;
    }

    public void setType(DatasourceType type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
