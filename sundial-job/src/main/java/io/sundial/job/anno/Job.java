package io.sundial.job.anno;

import java.lang.annotation.*;

/**
 * 作业注解
 *
 * @author Payne 646742615@qq.com
 * 2019/1/8 22:17
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface Job {

    /**
     * 作业名称，覆盖{@link io.sundial.job.Job#name()}
     *
     * @return 作业名称
     */
    String value();

    /**
     * 作业分组，覆盖{@link io.sundial.job.Job#group()}
     *
     * @return 作业分组
     */
    String group() default "";

    /**
     * 作业版本，覆盖{@link io.sundial.job.Job#version()}
     *
     * @return 作业版本
     */
    String version() default "";

    /**
     * 作业类型，覆盖{@link io.sundial.job.Job#type()}
     *
     * @return 作业类型
     */
    String type() default "";

    /**
     * 作业最大分片数量，覆盖{@link io.sundial.job.Job#shardable()}
     *
     * @return 作业名称
     */
    int shardable() default 0;

    /**
     * 作业名称，覆盖{@link io.sundial.job.Job#name()}
     *
     * @return 作业名称
     */
    String description() default "";

}
