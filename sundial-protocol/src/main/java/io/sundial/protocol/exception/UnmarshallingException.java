package io.sundial.protocol.exception;

import io.sundial.protocol.ProtocolException;

/**
 * 解码异常
 *
 * @author Payne 646742615@qq.com
 * 2018/12/24 15:44
 */
public class UnmarshallingException extends ProtocolException {
    private static final long serialVersionUID = -805692529691820808L;

    public UnmarshallingException() {
    }

    public UnmarshallingException(String message) {
        super(message);
    }

    public UnmarshallingException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnmarshallingException(Throwable cause) {
        super(cause);
    }
}
