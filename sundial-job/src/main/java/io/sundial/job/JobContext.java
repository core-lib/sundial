package io.sundial.job;

import io.sundial.core.context.Clazz;
import io.sundial.core.context.Context;
import io.sundial.core.context.RoleException;
import io.sundial.core.context.Supplier;

import java.util.Iterator;
import java.util.Map;

/**
 * 作业参数
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 12:07
 */
public class JobContext implements Context {
    private final long jobTime;
    private final int shardingIndex;
    private final int shardingTotal;
    private final String shardingValue;
    private final Map<String, String> jobArguments;
    private final Context context;

    public JobContext(long jobTime, int shardingIndex, int shardingTotal, String shardingValue, Map<String, String> jobArguments, Context context) {
        this.jobTime = jobTime;
        this.shardingIndex = shardingIndex;
        this.shardingTotal = shardingTotal;
        this.shardingValue = shardingValue;
        this.jobArguments = jobArguments;
        this.context = context;
    }

    public long getJobTime() {
        return jobTime;
    }

    public int getShardingIndex() {
        return shardingIndex;
    }

    public int getShardingTotal() {
        return shardingTotal;
    }

    public String getShardingValue() {
        return shardingValue;
    }

    public Map<String, String> getJobArguments() {
        return jobArguments;
    }

    @Override
    public <T> T get(String name) throws RoleException, ClassCastException {
        return context.get(name);
    }

    @Override
    public <T> T get(String name, Supplier<T> supplier) throws RoleException, ClassCastException {
        return context.get(name, supplier);
    }

    @Override
    public <T> T get(Class<T> type) throws RoleException {
        return context.get(type);
    }

    @Override
    public <T> T get(Class<T> type, Supplier<T> supplier) throws RoleException {
        return context.get(type, supplier);
    }

    @Override
    public <T> T get(Clazz<T> type, Supplier<T> supplier) throws RoleException {
        return context.get(type, supplier);
    }

    @Override
    public Class<?> check(String name) throws RoleException {
        return context.check(name);
    }

    @Override
    public <T> Map<String, T> fetch(Class<T> type) throws RoleException {
        return context.fetch(type);
    }

    @Override
    public Iterator<String> iterator() {
        return context.iterator();
    }

}
