package io.sundial.application.impl;

import io.sundial.application.Application;
import io.sundial.core.context.Context;

/**
 * 应用启动器
 *
 * @author Payne 646742615@qq.com
 * 2019/1/23 11:00
 */
public class AppStarter {

    public static void main(String... args) throws Exception {
        Context context = new StandaloneContext();
        Application application = context.get(Application.class);
        application.initialize(context);
        application.startup();
    }

}
