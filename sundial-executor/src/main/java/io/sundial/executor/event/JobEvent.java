package io.sundial.executor.event;

import com.google.common.base.Preconditions;
import io.sundial.executor.ExecutorEvent;
import io.sundial.job.JobCommand;

/**
 * 作业事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/26 12:19
 */
public abstract class JobEvent extends ExecutorEvent {
    private final JobCommand jobCommand;

    protected JobEvent(JobCommand jobCommand) {
        Preconditions.checkNotNull(jobCommand, "jobCommand must not be null");
        this.jobCommand = jobCommand;
    }

    public JobCommand getJobCommand() {
        return jobCommand;
    }
}
