package io.sundial.console.task.web;

import io.sundial.console.task.core.Logging;

import javax.validation.constraints.Min;

/**
 * 执行记录搜索参数
 *
 * @author Payne 646742615@qq.com
 * 2019/1/14 13:31
 */
public class LoggingSearchParam {
    private String dateStart;
    private String dateEnd;
    private Logging.Status status;
    /**
     * 页码，从1开始
     */
    @Min(value = 1, message = "页码不能小于1")
    private int pageNo;

    /**
     * 每页最多显示条目，不能小于1
     */
    @Min(value = 1, message = "每页最多显示条数不能小于1")
    private int pageSize;

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Logging.Status getStatus() {
        return status;
    }

    public void setStatus(Logging.Status status) {
        this.status = status;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
