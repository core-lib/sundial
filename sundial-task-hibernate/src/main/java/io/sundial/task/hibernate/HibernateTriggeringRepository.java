package io.sundial.task.hibernate;

import io.sundial.hibernate.SessionFactoryRepository;
import io.sundial.hibernate.TrxRNCall;
import io.sundial.hibernate.TrxVNCall;
import io.sundial.hibernate.TrxVTCall;
import io.sundial.repository.RepositoryException;
import io.sundial.task.Triggering;
import io.sundial.task.TriggeringRepository;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Enumeration;

/**
 * 触发记录仓储Hibernate实现
 *
 * @author Payne 646742615@qq.com
 * 2018/12/29 17:09
 */
public class HibernateTriggeringRepository extends SessionFactoryRepository implements TriggeringRepository {

    @Override
    public Triggering find(final Triggering.Key key) {
        return doReadonly(new TrxRNCall<Triggering>() {
            @Override
            public Triggering call(Session sex) {
                return sex.get(Triggering.class, new Triggering(key));
            }
        });
    }

    @Override
    public void save(final Triggering.Key key, final Triggering value) throws RepositoryException {
        doTransaction(new TrxVTCall<RepositoryException>() {
            @Override
            public void call(Session sex) {
                sex.save(value);
            }
        });
    }

    @Override
    public void replace(Triggering.Key key, Triggering value) throws RepositoryException {

    }

    @Override
    public Triggering remove(Triggering.Key key) {
        return null;
    }

    @Override
    public boolean contains(Triggering.Key key) {
        return false;
    }

    @Override
    public Enumeration<Triggering.Key> keys() {
        return null;
    }

    @Override
    public Enumeration<Triggering> values() {
        return null;
    }

    @Override
    public void toSubmitted(final Triggering.Key key) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toSubmitted", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.executeUpdate();
            }
        });
    }

    @Override
    public void toRejected(final Triggering.Key key) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toRejected", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.executeUpdate();
            }
        });
    }

    @Override
    public void toCanceled(final Triggering.Key key) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toCanceled", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.executeUpdate();
            }
        });
    }

    @Override
    public void toExecuting(final Triggering.Key key) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toExecuting", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.executeUpdate();
            }
        });
    }

    @Override
    public void toCompleted(final Triggering.Key key, final Integer code, final String message, final String result) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toCompleted", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.setParameter("code", code);
                query.setParameter("message", message);
                query.setParameter("result", result);
                query.executeUpdate();
            }
        });
    }

    @Override
    public void toFailed(final Triggering.Key key, final String result) {
        doTransaction(new TrxVNCall() {
            @Override
            public void call(Session sex) {
                Query<Object> query = sex.createNamedQuery("Triggering-toFailed", null);
                query.setParameter("taskName", key.getTaskName());
                query.setParameter("taskGroup", key.getTaskGroup());
                query.setParameter("time", key.getTime());
                query.setParameter("shardingIndex", key.getShardingIndex());
                query.setParameter("result", result);
                query.executeUpdate();
            }
        });
    }
}
