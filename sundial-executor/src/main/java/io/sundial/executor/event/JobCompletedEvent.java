package io.sundial.executor.event;

import io.sundial.job.JobCommand;
import io.sundial.job.JobResult;

/**
 * 作业命令已完成事件
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 17:13
 */
public class JobCompletedEvent extends JobEvent {
    private final JobResult jobResult;

    public JobCompletedEvent(JobCommand jobCommand, JobResult jobResult) {
        super(jobCommand);
        this.jobResult = jobResult;
    }

    public JobResult getJobResult() {
        return jobResult;
    }
}
