package io.sundial.executor;

import io.sundial.engine.Engine;
import io.sundial.executor.exception.ExecutingException;
import io.sundial.job.JobCommand;
import io.sundial.job.JobExecution;

import java.io.Serializable;

/**
 * 执行器
 *
 * @author Payne 646742615@qq.com
 * 2018/12/20 11:36
 */
public interface Executor extends Engine {

    /**
     * 执行作业命令
     *
     * @param jobCommand 作业命令
     * @return 作业执行回执
     * @throws ExecutingException 执行异常
     */
    JobExecution execute(JobCommand jobCommand) throws ExecutingException;

    /**
     * 取消作业命令
     *
     * @param jobCommand            作业命令
     * @param mayInterruptIfRunning 是否中断如果作业命令在运行中
     * @return {@code false}如果命令无法取消，通常情况下是因为命令已执行， 否则返回{@code true}
     */
    boolean cancel(JobCommand jobCommand, boolean mayInterruptIfRunning);

    /**
     * 执行器实时信息
     * 用于保存执行器当前的状态信息，包括任务负荷，已执行任务数量，成功数，失败数等信息。
     */
    class Information implements Serializable {
        private static final long serialVersionUID = -6021213725973633895L;

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
